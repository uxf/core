<?php

declare(strict_types=1);

namespace UXF\CoreTests\Story;

use UXF\Core\Test\WebTestCase;

class ErrorTest extends WebTestCase
{
    public function testResponseModifier(): void
    {
        $client = self::createClient([
            'debug' => false,
            'environment' => 'prod',
        ]);

        $client->get('/error/basic');
        self::assertResponseStatusCodeSame(400);
        self::assertSame([
            'error' => [
                'code' => 'ERROR_CODE',
                'message' => 'message',
            ],
            'validationErrors' => [],
        ], $client->getResponseData());

        $client->get('/error/validation');
        self::assertResponseStatusCodeSame(400);
        self::assertSame([
            'error' => [
                'code' => 'BAD_USER_INPUT',
                'message' => 'INVALID INPUT',
            ],
            'validationErrors' => [[
                'field' => 'x',
                'message' => 'problem',
            ]],
        ], $client->getResponseData());

        $client->get('/error/validation-array');
        self::assertResponseStatusCodeSame(400);
        self::assertSame([
            'error' => [
                'code' => 'BAD_USER_INPUT',
                'message' => 'INVALID INPUT',
            ],
            'validationErrors' => [
                [
                    'field' => 'a.X',
                    'message' => 'problem 1',
                ],
                [
                    'field' => 'b.Y',
                    'message' => 'problem 2',
                ],
                [
                    'field' => 'b.Y',
                    'message' => 'problem 3',
                ],
            ],
        ], $client->getResponseData());

        $client->get('/error/validation-list');
        self::assertResponseStatusCodeSame(400);
        self::assertSame([
            'error' => [
                'code' => 'BAD_USER_INPUT',
                'message' => 'INVALID INPUT',
            ],
            'validationErrors' => [
                [
                    'field' => '0:X',
                    'message' => 'problem 1',
                ],
                [
                    'field' => '1:Y',
                    'message' => 'problem 2',
                ],
                [
                    'field' => '1:Y',
                    'message' => 'problem 3',
                ],
            ],
        ], $client->getResponseData());

        $client->get('/error/http');
        self::assertResponseStatusCodeSame(401);
        self::assertSame([
            'error' => [
                'code' => 'UNAUTHORIZED',
                'message' => 'UNAUTHORIZED',
            ],
            'validationErrors' => [],
        ], $client->getResponseData());

        $client->get('/error/firewall');
        self::assertResponseStatusCodeSame(401);
        self::assertSame([
            'error' => [
                'code' => 'UNAUTHORIZED',
                'message' => 'UNAUTHORIZED',
            ],
            'validationErrors' => [],
        ], $client->getResponseData());

        $client->get('/error/fatal');
        self::assertResponseStatusCodeSame(500);
        self::assertSame([
            'error' => [
                'code' => 'SERVER_ERROR',
                'message' => 'SERVER ERROR',
            ],
            'validationErrors' => [],
        ], $client->getResponseData());

        $client->post('/error/method', []);
        self::assertResponseStatusCodeSame(405);
        self::assertSame([
            'error' => [
                'code' => 'BAD_REQUEST',
                'message' => 'BAD_REQUEST',
            ],
            'validationErrors' => [],
        ], $client->getResponseData());

        $client->get('/not-exists');
        self::assertResponseStatusCodeSame(404);
        self::assertSame([
            'error' => [
                'code' => 'NOT_FOUND',
                'message' => 'NOT_FOUND',
            ],
            'validationErrors' => [],
        ], $client->getResponseData());
    }
}

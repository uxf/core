<?php

declare(strict_types=1);

namespace UXF\CoreTests\Unit\SystemProvider;

use PHPUnit\Framework\TestCase;
use UXF\Core\SystemProvider\Calendar;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;

class CalendarTest extends TestCase
{
    public function test(): void
    {
        $today = Date::createFromDateTime(new DateTime());
        self::assertSame((string) $today, (string) Calendar::today());

        // freeze
        Calendar::$frozenDate = new Date('2010-01-05');
        self::assertSame((string) new Date('2010-01-05'), (string) Calendar::today());

        // unfreeze
        Calendar::$frozenDate = null;
        self::assertSame((string) $today, (string) Calendar::today());
    }
}

<?php

declare(strict_types=1);

namespace UXF\CoreTests\Unit\SystemProvider;

use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid as RamseyUuid;
use UXF\Core\SystemProvider\Clock;
use UXF\Core\SystemProvider\Uuid;
use UXF\Core\Type\DateTime;

class UuidTest extends TestCase
{
    public function testUuid4(): void
    {
        self::assertNotSame((string) RamseyUuid::fromString('00000000-0000-4000-0000-000000000001'), (string) Uuid::uuid4());

        // start sequence
        Uuid::$seq = 1;
        self::assertSame((string) RamseyUuid::fromString('00000000-0000-4000-0000-000000000001'), (string) Uuid::uuid4());
        self::assertSame((string) RamseyUuid::fromString('00000000-0000-4000-0000-000000000002'), (string) Uuid::uuid4());
        self::assertSame((string) RamseyUuid::fromString('00000000-0000-4000-0000-000000000003'), (string) Uuid::uuid4());

        // clear sequence
        Uuid::$seq = null;
        self::assertNotSame((string) RamseyUuid::fromString('00000000-0000-4000-0000-000000000004'), (string) Uuid::uuid4());
    }

    public function testUuid7(): void
    {
        self::assertNotSame((string) RamseyUuid::fromString('017e14dd-fe80-7000-0000-000000000001'), (string) Uuid::uuid7());

        // start sequence
        Clock::$frozenTime = new DateTime('2022-01-01T10:00:00+01:00');
        Uuid::$seq = 1;
        self::assertSame((string) RamseyUuid::fromString('017e14dd-fe80-7000-0000-000000000001'), (string) Uuid::uuid7());
        self::assertSame((string) RamseyUuid::fromString('017e14dd-fe80-7000-0000-000000000002'), (string) Uuid::uuid7());
        self::assertSame((string) RamseyUuid::fromString('017e14dd-fe80-7000-0000-000000000003'), (string) Uuid::uuid7());

        // clear sequence
        Clock::$frozenTime = null;
        Uuid::$seq = null;
        self::assertNotSame((string) RamseyUuid::fromString('017e14dd-fe80-7000-0000-000000000004'), (string) Uuid::uuid7());
    }
}

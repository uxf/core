<?php

declare(strict_types=1);

namespace UXF\CoreTests\Unit;

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use UXF\Core\Utils\ClassNameHelper;
use UXF\CoreTests\Project\Entity\NumericEnum;
use UXF\CoreTests\Project\Entity\State;
use UXF\CoreTests\Project\Fake;

class ClassNameHelperTest extends TestCase
{
    #[DataProvider('getData')]
    public function test(?string $expected, string $content): void
    {
        self::assertSame($expected, ClassNameHelper::resolve($content));
    }

    public function testShortName(): void
    {
        self::assertSame('NumericEnum', ClassNameHelper::shortName(NumericEnum::class));
    }

    /**
     * @return mixed[]
     */
    public static function getData(): array
    {
        return [
            'empty' => [
                null,
                '',
            ],
            'not class' => [
                null,
                'namespace UXF\CoreTests\Project;
class Faker
{}',
            ],
            'simple class' => [
                Fake::class,
                'namespace UXF\CoreTests\Project;
class Fake
{}',
            ],
            'final class' => [
                Fake::class,
                'namespace UXF\CoreTests\Project;
final class Fake
{}',
            ],
            'abstract class' => [
                Fake::class,
                'namespace UXF\CoreTests\Project;
abstract class Fake
{}',
            ],
            'readonly class' => [
                Fake::class,
                'namespace UXF\CoreTests\Project;
readonly class Fake
{}',
            ],
            'readonly final class' => [
                Fake::class,
                'namespace UXF\CoreTests\Project;
readonly final class Fake
{}',
            ],
            'implements class' => [
                Fake::class,
                'namespace UXF\CoreTests\Project;
class Fake implements extends
{}',
            ],
            'enum simple' => [
                State::class,
                'namespace UXF\CoreTests\Project\Entity;
enum State
',
            ],
            'enum string' => [
                State::class,
                'namespace UXF\CoreTests\Project\Entity;
enum State: string
',
            ],
            'enum int' => [
                State::class,
                'namespace UXF\CoreTests\Project\Entity;
enum State: int
',
            ],
        ];
    }
}

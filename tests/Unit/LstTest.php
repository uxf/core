<?php

declare(strict_types=1);

namespace UXF\CoreTests\Unit;

use PHPUnit\Framework\TestCase;
use UXF\Core\Utils\Lst;
use UXF\CoreTests\Project\Entity\Language;
use UXF\CoreTests\Project\Entity\State;
use function Safe\json_encode;

class LstTest extends TestCase
{
    public function testCreate(): void
    {
        $expected = ['a', 'b'];

        $values = ['a', 'b'];
        self::assertSame($expected, Lst::from($values)->getValues());

        $values = [
            1 => 'a',
            0 => 'b',
        ];
        self::assertSame($expected, Lst::from($values)->getValues());

        $values = call_user_func(static function (): iterable {
            yield 'ok' => 'a';
            yield 'or' => 'b';
        });
        self::assertSame($expected, Lst::from($values)->getValues());
    }

    public function testFind(): void
    {
        $list = Lst::from([1, 2, 3]);
        self::assertSame(2, $list->find(fn (int $v) => $v === 2));
        self::assertNull($list->find(fn (int $v) => $v === 4));
    }

    public function testMap(): void
    {
        $list = Lst::from(['A', 'B']);
        self::assertSame(['A', 'B'], $list->map(fn (string $item, int $index) => $item)->getValues());
        self::assertSame(["A0", "B1"], $list->map(fn (string $item, int $index) => "{$item}{$index}")->getValues());
    }

    public function testFilter(): void
    {
        $list = Lst::from([1, 2, 3]);
        self::assertSame([2, 3], $list->filter(fn (int $v) => $v >= 2)->getValues());
    }

    public function testSort(): void
    {
        $list = Lst::from([3, 1, -10]);
        self::assertSame([-10, 1, 3], $list->sort()->getValues());
    }

    public function testUnique(): void
    {
        $list = Lst::from([3, 3]);
        self::assertSame([3], $list->unique()->getValues());

        // weird
        $list = Lst::from([3, '3']);
        self::assertSame([3], $list->unique(SORT_NUMERIC)->getValues());
        self::assertSame([3], $list->unique(SORT_STRING)->getValues());

        $l1 = Language::create(10, 'X');
        $l2 = Language::create(10, 'Y');
        $l3 = $l1;
        $list = Lst::from([$l1, $l2, $l3]);
        self::assertSame([$l1, $l2], $list->unique()->getValues());
        self::assertSame([$l1], $list->unique(SORT_STRING)->getValues());
    }

    public function testConcat(): void
    {
        $list = Lst::from([3, 1]);
        $list2 = Lst::from([10, 20]);
        self::assertSame([3, 1, 10, 20], $list->concat($list2)->getValues());
    }

    public function testPush(): void
    {
        $list = Lst::from([3, 1]);
        self::assertSame([3, 1, 4], $list->push(4)->getValues());
    }

    public function testUnshift(): void
    {
        $list = Lst::from([3, 1]);
        self::assertSame([4, 3, 1], $list->unshift(4)->getValues());
    }

    public function testSlice(): void
    {
        $list = Lst::from([10, 11, 12, 13]);
        self::assertSame([10, 11, 12, 13], $list->slice(0)->getValues());
        self::assertSame([11, 12, 13], $list->slice(1)->getValues());
        self::assertSame([11, 12], $list->slice(1, 2)->getValues());
        self::assertSame([13], $list->slice(-1)->getValues());
    }

    public function testJoin(): void
    {
        $list = Lst::from([1, 2, 3]);
        self::assertSame('123', $list->join());
        self::assertSame('1, 2, 3', $list->join(', '));
    }

    public function testAggregate(): void
    {
        $list = Lst::from([1, 2, 3]);
        self::assertSame(6, $list->aggregate(0, fn (int $sum, int $item) => $sum + $item));

        $list = Lst::from(['a', 'b', 'c']);
        self::assertSame('abc', $list->aggregate('', fn (string $prev, string $item) => $prev . $item));
    }

    public function testForEach(): void
    {
        $test = new class() {
            public function __construct(
                public int $counter = 0,
                public string $buffer = '',
            ) {
            }
        };

        Lst::from([1, 2, 3])->forEach(static fn (int $item) => $test->counter += $item);
        self::assertSame(6, $test->counter);

        Lst::from([1, 2, 3])->forEach(static fn (int $item, int $index) => $test->buffer .= $index);
        self::assertSame('012', $test->buffer);
    }

    public function testDictionary(): void
    {
        $list = Lst::from([
            Language::create(10, 'A'),
            Language::create(11, 'B'),
            Language::create(12, 'C'),
        ]);
        self::assertSame([
            10 => 'A',
            11 => 'B',
            12 => 'C',
        ], $list->dictionary(fn (Language $l) => $l->getId(), fn (Language $l) => $l->getName()));
    }

    public function testGroupBy(): void
    {
        $list = Lst::from([
            Language::create(10, 'A1'),
            Language::create(10, 'A2'),
            Language::create(11, 'B'),
        ]);
        self::assertSame([
            10 => ['A1', 'A2'],
            11 => ['B'],
        ], $list->groupBy(fn (Language $l) => $l->getId(), fn (Language $l) => $l->getName()));
    }

    public function testFirst(): void
    {
        $list = Lst::from([1, 2, 3]);
        self::assertSame(1, $list->first());
        self::assertSame(1, $list->first());

        /** @var array<mixed> $tmp */
        $tmp = [];
        $list = Lst::from($tmp);
        self::assertNull($list->first());
    }

    public function testLast(): void
    {
        $list = Lst::from([1, 2, 3]);
        self::assertSame(3, $list->last());
        self::assertSame(3, $list->last());

        /** @var array<mixed> $tmp */
        $tmp = [];
        $list = Lst::from($tmp);
        self::assertNull($list->last());
    }

    public function testCount(): void
    {
        self::assertSame(0, Lst::from([])->count());
        self::assertSame(2, Lst::from([1, 2])->count());
    }

    public function testIsEmpty(): void
    {
        self::assertTrue(Lst::from([])->isEmpty());
        self::assertFalse(Lst::from([
            1 => 0,
        ])->isEmpty());
    }

    public function testContains(): void
    {
        $l1 = Language::create(10, 'A1');
        $l2 = Language::create(10, 'A1');
        self::assertTrue(Lst::from([1])->contains(1));
        self::assertFalse(Lst::from([1])->contains(2));
        self::assertTrue(Lst::from([$l1])->contains($l1));
        self::assertFalse(Lst::from([$l1])->contains($l2));
    }

    public function testJsonSerializable(): void
    {
        self::assertSame(
            '["NEW"]',
            json_encode(Lst::from([State::NEW])),
        );
    }

    public function testPhpstanPure(): void
    {
        $lst = Lst::from([1]);
        $lst->map(fn (int $x) => $x); // @phpstan-ignore-line
        $lst->filter(fn (int $x) => $x > 0); // @phpstan-ignore-line
        $lst->sort(); // @phpstan-ignore-line
        $lst->unique(); // @phpstan-ignore-line
        $lst->concat($lst); // @phpstan-ignore-line
        $lst->push(1); // @phpstan-ignore-line
        $lst->unshift(1); // @phpstan-ignore-line
        $lst->slice(0, 0); // @phpstan-ignore-line
        self::assertSame([1], $lst->getValues());
    }

    public function testFull(): void
    {
        $values = Lst::from([
            Language::create(10, 'Z'),
            Language::create(11, 'A'),
            Language::create(12, 'C'),
            Language::create(13, 'A'),
            Language::create(14, 'D'),
        ])
            ->filter(fn (Language $l) => $l->getId() >= 12)
            ->sort(fn (Language $a, Language $b) => $a->getName() <=> $b->getName())
            ->map(fn (Language $l) => $l->getName())
            ->unique()
            ->getValues();

        self::assertSame(['A', 'C', 'D'], $values);
    }
}

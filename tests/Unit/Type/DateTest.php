<?php

declare(strict_types=1);

namespace UXF\CoreTests\Unit\Type;

use DateInterval;
use Exception;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;

class DateTest extends TestCase
{
    public function test(): void
    {
        $dateA = new Date('2020-01-02');
        $dateB = new Date('2021-01-02');
        $dateC = new Date('2021-01-02');

        // ===
        self::assertTrue($dateB->equals($dateC));
        self::assertTrue($dateB->any($dateC));
        self::assertFalse($dateA->equals($dateB));
        self::assertFalse($dateA->any($dateB));
        self::assertFalse($dateA->equals(null));

        // >
        self::assertTrue($dateB > $dateA);
        self::assertTrue($dateB > null);
        self::assertFalse($dateA > $dateB);
        self::assertFalse($dateC > $dateB);

        // <
        self::assertTrue($dateA < $dateB);
        self::assertTrue(null < $dateB);
        self::assertFalse($dateB < $dateA);
        self::assertFalse($dateB < $dateC);

        // >=
        self::assertTrue($dateB >= $dateA);
        self::assertTrue($dateB >= null);
        self::assertFalse($dateA >= $dateB);

        // <=
        self::assertTrue($dateA <= $dateB);
        self::assertTrue(null <= $dateB);
        self::assertFalse($dateB <= $dateA);

        // add/sub days
        self::assertEquals('2020-01-12', $dateA->addDays(10));
        self::assertEquals('2019-12-23', $dateA->subDays(10));

        self::assertEquals('2020-01-12', $dateA->add(new DateInterval('P10D')));
        self::assertEquals('2019-12-23', $dateA->sub(new DateInterval('P10D')));

        // format
        self::assertSame('2. 1. 2020', $dateA->format('j. n. Y'));

        // serialize
        $date = new Date('2020-01-02');
        self::assertEquals($date, unserialize(serialize($date)));

        $date = Date::createFromFormat('j.n.Y', '2.1.1992');
        self::assertEquals('1992-01-02', $date);

        $date = Date::createFromFormat('Y-m-d\TH:i', '1992-01-02T00:00');
        self::assertEquals('1992-01-02', $date);

        $dateTime = new DateTime('2022-09-27T00:00:00+02:00');
        $date = Date::createFromDateTime($dateTime);
        self::assertEquals('2022-09-27', $date);
        self::assertEquals('2022-09-27T00:00:00+02:00', $date->toDateTime());
    }

    public function testDiff(): void
    {
        $dateA = new Date('2021-01-02');
        $dateB = new Date('2021-01-10');

        self::assertSame(0, $dateA->diff($dateA)->d);
        self::assertSame(8, $dateA->diff($dateB)->d);
        self::assertSame(8, $dateB->diff($dateA)->d);
    }

    public function testDiffDays(): void
    {
        $dateA = new Date('2021-01-02');
        $dateB = new Date('2021-01-10');
        $dateC = new Date('2024-01-10');

        self::assertSame(0, $dateA->diffDays($dateA));
        // other is in future: return positive
        self::assertSame(8, $dateA->diffDays($dateB));
        // other is in past:   return negative
        self::assertSame(-8, $dateB->diffDays($dateA));

        self::assertSame(1103, $dateA->diffDays($dateC));
        self::assertSame(-1103, $dateC->diffDays($dateA));
    }

    #[DataProvider('getInvalidData')]
    public function testInvalid(string $date): void
    {
        $this->expectException(Exception::class);
        new Date($date);
    }

    #[DataProvider('getInvalidFormatData')]
    public function testInvalidFormat(string $format, string $date): void
    {
        $this->expectException(Exception::class);
        Date::createFromFormat($format, $date);
    }

    /**
     * @return string[][]
     */
    public static function getInvalidData(): array
    {
        return [
            ['1.1.2020'],
            ['2020-02-30'],
            ['2020-2-3'],
            ['first day of month'],
        ];
    }

    /**
     * @return string[][]
     */
    public static function getInvalidFormatData(): array
    {
        return [
            ['Y-m-d', '2020-0-0'],
            ['Y-m-d', '2020-02-30'],
            ['j.n.Y', '12..13.2020'],
            ['Y', 'first day of month'],
        ];
    }
}

<?php

declare(strict_types=1);

namespace UXF\CoreTests\Unit\Type;

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use UXF\Core\Type\Url;

class UrlTest extends TestCase
{
    public function testSimple(): void
    {
        $url = Url::of('https://google.com');
        $url2 = Url::of('https://google.com');
        self::assertTrue($url->equals($url2));
        self::assertTrue($url->any($url2));

        $url3 = Url::of('https://seznam.cz');
        self::assertFalse($url->equals($url3));
        self::assertFalse($url->any($url3));
    }

    public function testParseWithoutDomainCheck(): void
    {
        $url = Url::parseWithoutDomainCheck('local://default/dir');
        self::assertSame('local://default/dir', $url->toString());
    }

    #[DataProvider('getData')]
    public function test(string $actual, string $expected): void
    {
        self::assertSame($expected, Url::of($actual)->toString());
    }

    /**
     * @return string[][]
     */
    public static function getData(): array
    {
        return [
            ['https://google.com', 'https://google.com'],
            ['http://google.com', 'http://google.com'],
            ['http://127.0.0.1:3000', 'http://127.0.0.1:3000'],
            ['google.com', 'https://google.com'],
            [' google.com/hello?ok ', 'https://google.com/hello?ok'],
            [' google.com/hello?ok&test[]=1#fragment ', 'https://google.com/hello?ok&test[]=1#fragment'],
        ];
    }

    #[DataProvider('getData2')]
    public function testValid(string $actual, bool $expected): void
    {
        self::assertSame($expected, Url::tryParse($actual) !== null);
    }

    /**
     * @return array<array{string, bool}>
     */
    public static function getData2(): array
    {
        return [
            ['x', false],
            ['', false],
            ['https://šš.šš', false],
            ['https://uxf.cz', true],
        ];
    }

    public function testComponents(): void
    {
        $components = Url::of('https://user:pass@google.com:443/hello?ok&test[]=1#fragment')->getComponents();
        self::assertSame('https', $components->scheme);
        self::assertSame('google.com', $components->host);
        self::assertSame(443, $components->port);
        self::assertSame('user', $components->user);
        self::assertSame('pass', $components->pass);
        self::assertSame('/hello', $components->path);
        self::assertSame([
            'ok' => '',
            'test' => ['1'],
        ], $components->query);
        self::assertSame('fragment', $components->fragment);

        $components = Url::of('localhost')->getComponents();
        self::assertSame('https', $components->scheme);
        self::assertSame('localhost', $components->host);
        self::assertNull($components->port);
        self::assertNull($components->user);
        self::assertNull($components->pass);
        self::assertNull($components->path);
        self::assertEmpty($components->query);
        self::assertNull($components->fragment);
    }
}

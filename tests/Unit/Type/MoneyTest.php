<?php

declare(strict_types=1);

namespace UXF\CoreTests\Unit\Type;

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use UXF\Core\Exception\MoneyException;
use UXF\Core\Type\Currency;
use UXF\Core\Type\Decimal;
use UXF\Core\Type\Money;

class MoneyTest extends TestCase
{
    public function test(): void
    {
        $c = Currency::CZK;

        self::assertSame('1', Money::of(1, $c)->amount);
        self::assertSame('1.01', Money::of(1.01, $c)->amount);
        self::assertSame('1', Money::of(1.001, $c)->amount);
        self::assertSame('-1', Money::of(-1.001, $c)->amount);
        self::assertSame('-1', Money::of(-1, $c)->amount);
        self::assertSame('-1.12', Money::of(-1.12, $c)->amount);
        self::assertSame('0', Money::of('0.0000', $c)->amount);
        self::assertSame('0', Money::of('-0.0000', $c)->amount);
        self::assertSame('10', Money::of('10.0000', $c)->amount);
        self::assertSame('-10', Money::of('-10.0000', $c)->amount);
        self::assertSame('-10.1', Money::of('-10.1000', $c)->amount);
        self::assertSame('1000000000000.01', Money::of('1000000000000.01', $c)->amount);
        self::assertSame('-1000000000000.01', Money::of('-1000000000000.01', $c)->amount);
        self::assertSame('1', Money::of(Decimal::of(1), $c)->amount);

        // round
        self::assertSame('1.05', Money::of(1.046, $c)->amount);
        self::assertSame('1.05', Money::of(1.045, $c)->amount);
        self::assertSame('1.04', Money::of(1.044, $c)->amount);
        self::assertSame('-1.05', Money::of(-1.046, $c)->amount);
        self::assertSame('-1.05', Money::of(-1.045, $c)->amount);
        self::assertSame('-1.04', Money::of(-1.044, $c)->amount);

        self::assertTrue(Money::of(10, $c) > Money::of(5, $c));
        self::assertTrue(Money::of(10, $c) >= Money::of(5, $c));
        self::assertTrue(Money::of(10.1, $c) < Money::of(100.1, $c));
        self::assertTrue(Money::of(0.1, $c) > Money::zero($c));
        self::assertTrue(Money::of(0, $c) >= Money::zero($c));
        self::assertTrue(Money::of('-1', $c) < Money::zero($c));
        self::assertTrue(Money::of(-1, $c) < Money::of(-0.99, $c));
        self::assertTrue(Money::of(-0.99, $c) > Money::of(-1, $c));
        self::assertTrue(Money::of(-1, $c) < Money::of(1, $c));
        self::assertTrue(Money::of(1, $c) > Money::of(-1, $c));
        self::assertTrue(Money::of(9, $c) > Money::of(-9.99, $c));
        self::assertTrue(Money::of(-9, $c) > Money::of(-9.99, $c));

        self::assertSame('7.99', Money::of(3.14, $c)->plus(Money::of(4.85, $c))->amount);
        self::assertSame('-1.71', Money::of(3.14, $c)->minus(Money::of(4.85, $c))->amount);
        self::assertSame('9.42', Money::of(3.14, $c)->multipliedBy(3)->amount);
        self::assertSame('0.79', Money::of(3.14, $c)->dividedBy(4)->amount);
        self::assertSame(3, Money::of(3.14, $c)->amountInt());
        self::assertSame(3.14, Money::of(3.14, $c)->amountFloat());

        self::assertTrue(Money::zero($c)->isZero());
        self::assertFalse(Money::of(1, $c)->isZero());

        self::assertTrue(Money::of(1, $c)->isPositive());
        self::assertTrue(Money::of(0, $c)->isPositiveOrZero());
        self::assertTrue(Money::of(1, $c)->isPositiveOrZero());
        self::assertTrue(Money::of(-1, $c)->isNegative());
        self::assertTrue(Money::of(0, $c)->isNegativeOrZero());
        self::assertTrue(Money::of(-1, $c)->isNegativeOrZero());

        self::assertTrue(Money::zero($c)->equals(Money::zero($c)));
        self::assertTrue(Money::zero($c)->any(Money::zero($c)));
        self::assertFalse(Money::zero($c)->any(Money::of(1, $c)));
        self::assertTrue(Money::of('0', $c)->equals(Money::of('-0', $c)));
        self::assertTrue(Money::of(3.14, $c)->equals(Money::of(3.14, $c)));
        self::assertFalse(Money::of(3.14, $c)->equals(Money::of(3.13, $c)));
        self::assertFalse(Money::of(3.14, $c)->equals(null));
        self::assertFalse(Money::of(3.14, $c)->equals(Money::of(3.13, Currency::EUR)));

        self::assertSame('0', Money::of(666, $c)->minus(Money::of(666, $c))->amount);

        self::assertTrue(Money::zero($c)->amountDecimal()->isZero());
    }

    #[DataProvider('getInvalidData')]
    public function testInvalid(string $amount): void
    {
        $this->expectException(MoneyException::class);
        $this->expectExceptionMessage('Invalid number format');
        Money::of($amount, Currency::CZK);
    }

    public function testCurrencyMismatchPlus(): void
    {
        $this->expectException(MoneyException::class);
        $this->expectExceptionMessage('Currency mismatch');
        $x = Money::of(1, Currency::CZK)->plus(Money::of(1, Currency::EUR));
    }

    public function testCurrencyMismatchMinus(): void
    {
        $this->expectException(MoneyException::class);
        $this->expectExceptionMessage('Currency mismatch');
        $x = Money::of(1, Currency::CZK)->minus(Money::of(1, Currency::EUR));
    }

    /**
     * @return string[][]
     */
    public static function getInvalidData(): array
    {
        return [
            [''],
            ['-'],
            ['a'],
            ['1.0.'],
            ['1..0'],
            ['+10'],
            ['--10'],
        ];
    }

    public function testPhpstanPure(): void
    {
        $a = Money::of(1, Currency::CZK);
        $a->plus($a); // @phpstan-ignore-line
        $a->minus($a); // @phpstan-ignore-line
        $a->multipliedBy(0); // @phpstan-ignore-line
        $a->dividedBy(1); // @phpstan-ignore-line
        self::assertSame(Currency::CZK, $a->currency);
    }
}

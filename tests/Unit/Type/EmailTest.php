<?php

declare(strict_types=1);

namespace UXF\CoreTests\Unit\Type;

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use UXF\Core\Type\Email;

class EmailTest extends TestCase
{
    public function testSimple(): void
    {
        $email = Email::of(' USER@UXF.CZ ');
        self::assertSame('user@uxf.cz', $email->toString());
        self::assertSame('user', $email->getLocalPart());
        self::assertSame('uxf.cz', $email->getDomain());

        $email2 = Email::of('user@uxf.cz');
        self::assertTrue($email->equals($email2));
        self::assertTrue($email->any($email2));

        $email3 = Email::of('user3@uxf.cz');
        self::assertFalse($email->equals($email3));
        self::assertFalse($email->any($email3));
    }

    #[DataProvider('getData')]
    public function test(string $actual, string $expected): void
    {
        self::assertSame($expected, Email::of($actual)->toString());
    }

    /**
     * @return string[][]
     */
    public static function getData(): array
    {
        return [
            ['Info@uxf.cz', 'info@uxf.cz'],
            ['XX@XX.XX', 'xx@xx.xx'],
            [' jak.jan0@ifire.cz', 'jak.jan0@ifire.cz'],
            ["dev@uxf.cz\n", 'dev@uxf.cz'],
            ["\thello+10.0@gmail.com", 'hello+10.0@gmail.com'],
            ['ab.cd#x+ef#x@uxf.cz', 'ab.cd#x+ef#x@uxf.cz'],
        ];
    }

    #[DataProvider('getData2')]
    public function testValid(string $actual, bool $expected): void
    {
        self::assertSame($expected, Email::tryParse($actual) !== null);
    }

    /**
     * @return array<array{string, bool}>
     */
    public static function getData2(): array
    {
        return [
            ['abc@y@cz.cz', false],
            ['', false],
            ['ŠĚŠČŘŽÝÁÍÉ@uxf.cz', false],
            ['a @uxf.cz', false],
            ['skoda@uxf.cz', true],
        ];
    }
}

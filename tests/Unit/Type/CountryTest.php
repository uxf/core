<?php

declare(strict_types=1);

namespace UXF\CoreTests\Unit\Type;

use PHPUnit\Framework\TestCase;
use UXF\Core\Type\Country;

class CountryTest extends TestCase
{
    public function test(): void
    {
        self::assertSame(Country::Czechia, Country::fromIsoCode('CZE'));
        self::assertSame('CZE', Country::Czechia->getIsoCode());
    }
}

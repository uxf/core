<?php

declare(strict_types=1);

namespace UXF\CoreTests\Unit\Type;

use Exception;
use PHPUnit\Framework\TestCase;
use UXF\Core\Type\DateTime;

class DateTimeTest extends TestCase
{
    public function test(): void
    {
        $date = new DateTime('2020-01-02');
        self::assertEquals($date, unserialize(serialize($date)));

        $dateA = new DateTime('2022-01-13T22:43:51+00:00');
        $dateB = new DateTime('2022-01-13T22:43:51+00:00');
        $dateC = new DateTime('2022-01-13T22:43:51+01:00');
        self::assertTrue($dateA->equals($dateB));
        self::assertTrue($dateA->any($dateB));
        self::assertFalse($dateA->equals($dateC));
        self::assertFalse($dateA->any($dateC));

        self::assertSame('2022-01-13T00:00:00+00:00', $dateA->resetToStartOfDay()->toString());
        self::assertSame('2022-01-13T00:00:00+01:00', $dateC->resetToStartOfDay()->toString());

        self::assertSame('2022-01-13T23:59:59+00:00', $dateA->resetToEndOfDay()->toString());
        self::assertSame('2022-01-13T23:59:59+01:00', $dateC->resetToEndOfDay()->toString());

        $dateD = new DateTime('2022-01-13T22:43:51+00:00');
        self::assertSame('2022-01-13T23:43:51+01:00', $dateD->normalizeTz()->toString());

        $this->expectException(Exception::class);
        new DateTime('2020-02-30');
    }
}

<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project\HttpQuery;

use UXF\CoreTests\Project\Entity\NumericEnum;

class FakeQuery
{
    /**
     * @param int[]|null $intArray
     * @param float[]|null $floatArray
     * @param bool[]|null $boolArray
     */
    public function __construct(
        public ?int $int = null,
        public ?float $float = null,
        public ?bool $bool = null,
        public ?NumericEnum $enum = null,
        public ?array $intArray = null,
        public ?array $floatArray = null,
        public ?array $boolArray = null,
    ) {
    }
}

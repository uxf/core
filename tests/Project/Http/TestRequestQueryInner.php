<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project\Http;

use Symfony\Component\Validator\Constraints as Assert;

class TestRequestQueryInner
{
    public function __construct(
        #[Assert\NotBlank]
        public ?string $hello = null,
        #[Assert\GreaterThan(0)]
        public ?int $hell = null,
    ) {
    }
}

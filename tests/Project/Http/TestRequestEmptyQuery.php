<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project\Http;

use UXF\CoreTests\Project\Entity\Language;

final readonly class TestRequestEmptyQuery
{
    public function __construct(
        public ?Language $language = null,
    ) {
    }
}

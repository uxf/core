<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project\Http;

use UXF\Core\Http\Request\NotSet;

final readonly class TestPatchBBody
{
    public function __construct(
        public string | null | NotSet $string = new NotSet(),
        public string | null | NotSet $stringNotSet = new NotSet(),
    ) {
    }
}

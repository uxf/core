<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project\Http;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

class LionParameterConverter implements ValueResolverInterface
{
    /**
     * @return InjectedObject[]
     */
    public function resolve(Request $request, ArgumentMetadata $argument): array
    {
        if ($argument->getType() !== InjectedObject::class) {
            return [];
        }

        return [new InjectedObject($request->getDefaultLocale())];
    }
}

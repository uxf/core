<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project\Controller;

use UXF\Core\Attribute\FromHeader;
use UXF\CoreTests\Project\HttpHeader\TestRequestHeader;

class HeaderController
{
    public function __invoke(#[FromHeader] TestRequestHeader $header): TestRequestHeader
    {
        return $header;
    }
}

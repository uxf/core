<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project\Controller;

use UXF\Core\Attribute\FromBody;

class BodyController
{
    public function __invoke(#[FromBody] mixed $body): mixed
    {
        return $body;
    }
}

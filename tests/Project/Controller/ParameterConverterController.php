<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project\Controller;

use Ramsey\Uuid\UuidInterface;
use Symfony\Component\HttpFoundation\Request;
use UXF\Core\Attribute\Entity;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Email;
use UXF\Core\Type\NationalIdentificationNumberCze;
use UXF\Core\Type\Phone;
use UXF\Core\Type\Time;
use UXF\CoreTests\Project\Entity\Hello;
use UXF\CoreTests\Project\Entity\State;
use UXF\CoreTests\Project\Entity\Wizard;
use UXF\CoreTests\Project\Http\InjectedObject;

class ParameterConverterController
{
    /**
     * /convert/{date}/{dateTime}/{time}/{email}/{phone}/{nin}/{enum}/{uuid}/{hello1}/{hello2}/{wizard}
     *
     * @return array<string, mixed>
     */
    public function __invoke(
        Request $request,
        InjectedObject $injectedObject,
        Date $date,
        DateTime $dateTime,
        Time $time,
        Email $email,
        Phone $phone,
        NationalIdentificationNumberCze $nin,
        State $enum,
        UuidInterface $uuid,
        Hello $hello1,
        #[Entity('uuid')] Hello $hello2,
        #[Entity('hidden')] Wizard $wizard,
    ): array {
        return [
            'injectedObject' => $injectedObject->defaultLocale,
            'date' => $date,
            'dateTime' => $dateTime,
            'time' => $time,
            'email' => $email,
            'phone' => $phone,
            'nin' => $nin,
            'enum' => $enum,
            'uuid' => $uuid,
            'hello1' => $hello1,
            'hello2' => $hello2,
            'wizard' => $wizard->name,
        ];
    }
}

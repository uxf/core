<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project\Dto;

use Ramsey\Uuid\UuidInterface;

final readonly class UuidDto
{
    /**
     * @param UuidInterface[] $a2
     * @param UuidInterface[] $b2
     * @param UuidInterface[] $c2
     */
    public function __construct(
        public UuidInterface $a1,
        public array $a2,
        public ?UuidInterface $b1,
        public ?array $b2,
        public ?UuidInterface $c1 = null,
        public ?array $c2 = [],
    ) {
    }
}

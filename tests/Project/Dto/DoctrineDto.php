<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project\Dto;

use UXF\Core\Attribute\Entity;
use UXF\CoreTests\Project\Entity\Hello;

final readonly class DoctrineDto
{
    /**
     * @param Hello[] $a2
     * @param Hello[] $b2
     * @param Hello[] $c2
     */
    public function __construct(
        public Hello $a1,
        public array $a2,
        #[Entity('uuid')] public ?Hello $b1,
        #[Entity('uuid')] public ?array $b2,
        #[Entity('uuid')] public ?Hello $c1 = null,
        #[Entity('uuid')] public ?array $c2 = [],
    ) {
    }
}

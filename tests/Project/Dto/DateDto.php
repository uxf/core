<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project\Dto;

use UXF\Core\Type\Date;

final readonly class DateDto
{
    /**
     * @param Date[] $a2
     * @param Date[] $b2
     * @param Date[] $c2
     */
    public function __construct(
        public Date $a1,
        public array $a2,
        public ?Date $b1,
        public ?array $b2,
        public ?Date $c1 = null,
        public ?array $c2 = [],
    ) {
    }
}

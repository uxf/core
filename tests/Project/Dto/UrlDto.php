<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project\Dto;

use UXF\Core\Type\Url;

final readonly class UrlDto
{
    /**
     * @param Url[] $a2
     * @param Url[] $b2
     * @param Url[] $c2
     */
    public function __construct(
        public Url $a1,
        public array $a2,
        public ?Url $b1,
        public ?array $b2,
        public ?Url $c1 = null,
        public ?array $c2 = [],
    ) {
    }
}

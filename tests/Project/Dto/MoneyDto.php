<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project\Dto;

use UXF\Core\Type\Money;

final readonly class MoneyDto
{
    /**
     * @param Money[] $a2
     * @param Money[] $b2
     * @param Money[] $c2
     */
    public function __construct(
        public Money $a1,
        public array $a2,
        public ?Money $b1,
        public ?array $b2,
        public ?Money $c1 = null,
        public ?array $c2 = [],
    ) {
    }
}

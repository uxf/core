<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project\Dto;

use UXF\Core\Type\Decimal;

final readonly class DecimalDto
{
    /**
     * @param Decimal[] $a2
     * @param Decimal[] $b2
     * @param Decimal[] $c2
     */
    public function __construct(
        public Decimal $a1,
        public array $a2,
        public ?Decimal $b1,
        public ?array $b2,
        public ?Decimal $c1 = null,
        public ?array $c2 = [],
    ) {
    }
}

<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project\Dto;

use UXF\Core\Type\DateTime;

final readonly class DateTimeDto
{
    /**
     * @param DateTime[] $a2
     * @param DateTime[] $b2
     * @param DateTime[] $c2
     */
    public function __construct(
        public DateTime $a1,
        public array $a2,
        public ?DateTime $b1,
        public ?array $b2,
        public ?DateTime $c1 = null,
        public ?array $c2 = [],
    ) {
    }
}

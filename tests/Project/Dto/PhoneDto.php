<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project\Dto;

use UXF\Core\Type\Phone;

final readonly class PhoneDto
{
    /**
     * @param Phone[] $a2
     * @param Phone[] $b2
     * @param Phone[] $c2
     */
    public function __construct(
        public Phone $a1,
        public array $a2,
        public ?Phone $b1,
        public ?array $b2,
        public ?Phone $c1 = null,
        public ?array $c2 = [],
    ) {
    }
}

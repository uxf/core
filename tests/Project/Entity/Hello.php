<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;
use UXF\Core\Type\BankAccountNumberCze;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\NationalIdentificationNumberCze;
use UXF\Core\Type\Phone;
use UXF\Core\Type\Time;

#[ORM\Entity]
class Hello
{
    public function __construct(
        #[ORM\Column, ORM\Id]
        public int $id,
        #[ORM\Column(type: 'uuid', unique: true)]
        public UuidInterface $uuid,
        #[ORM\Column(type: DateTime::class)]
        public DateTime $createdAt,
        #[ORM\Column(type: Date::class)]
        public Date $publishedAt,
        #[ORM\Column(type: Time::class)]
        public Time $openAt,
        #[ORM\Column(type: Phone::class)]
        public Phone $phone,
        #[ORM\Column(type: NationalIdentificationNumberCze::class)]
        public NationalIdentificationNumberCze $ninCze,
        #[ORM\Column(type: BankAccountNumberCze::class)]
        public BankAccountNumberCze $banCze,
    ) {
    }
}

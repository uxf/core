<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'wizard')]
#[ORM\InheritanceType('SINGLE_TABLE')]
#[ORM\DiscriminatorColumn('type', 'string')]
#[ORM\DiscriminatorMap([
    'w' => Wizard::class,
    's' => SuperWizard::class,
])]
abstract class SuperWizard
{
    #[ORM\Id, ORM\Column]
    private int $id = 0;

    #[ORM\Column]
    private int $hidden = 0;

    public function getId(): int
    {
        return $this->id;
    }

    public function getHidden(): int
    {
        return $this->hidden;
    }
}

<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Display
{
    #[ORM\Column, ORM\Id]
    public int $id = 0;

    /** @var string[] */
    #[ORM\Column(type: 'uxf_json', options: [
        'jsonb' => true,
    ])]
    public array $data = [];
}

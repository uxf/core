<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class WizardItem
{
    public function __construct(
        #[ORM\Id, ORM\Column]
        public int $id,
    ) {
    }
}

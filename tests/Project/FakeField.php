<?php

declare(strict_types=1);

namespace UXF\CoreTests\Project;

use UXF\Core\Attribute\Internal;

final readonly class FakeField
{
    #[Internal(FunnyService::class)]
    public function __construct()
    {
    }

    #[Internal(FunnyService::class)]
    public function forbidden(): void
    {
    }

    #[Internal(FunnyService::class)]
    public static function forbiddenStatic(): void
    {
    }
}

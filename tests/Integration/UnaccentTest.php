<?php

declare(strict_types=1);

namespace UXF\CoreTests\Integration;

use Doctrine\ORM\EntityManagerInterface;
use Nette\Utils\Strings;
use PHPUnit\Framework\Attributes\DataProvider;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use UXF\CoreTests\Project\Entity\Language;

class UnaccentTest extends KernelTestCase
{
    #[DataProvider('getData')]
    public function test(string $input): void
    {
        self::bootKernel();
        DbHelper::initDatabase(self::getContainer());

        /** @var EntityManagerInterface $em */
        $em = self::getContainer()->get(EntityManagerInterface::class);

        $language = $em->createQueryBuilder()
            ->select('l')
            ->from(Language::class, 'l')
            ->where('UNACCENT(l.name) LIKE UNACCENT(:text)')
            ->setParameter('text', $input)
            ->getQuery()
            ->getOneOrNullResult();

        self::assertInstanceOf(Language::class, $language);
    }

    #[DataProvider('getData')]
    public function testSearchHelper(string $input, string $expected): void
    {
        $actual = Strings::lower(Strings::toAscii($input));

        self::assertSame($expected, $actual);
    }

    /**
     * @return string[][]
     */
    public static function getData(): array
    {
        return [
            ['ěščřžýáíé', 'escrzyaie'],
            ['ŽLUŤOUČKÝ', 'zlutoucky'],
        ];
    }
}

<?php

declare(strict_types=1);

namespace UXF\CoreTests\Integration\Request;

use PHPUnit\Framework\TestCase;
use UXF\Core\Exception\ValidationException;
use UXF\Core\RequestConverter\DateParameterConverter;
use UXF\Core\Type\Date;

class DateParameterConverterTest extends TestCase
{
    public function testValid(): void
    {
        $actual = DateParameterConverter::convert('1992-04-15', '');
        self::assertEquals(new Date('1992-04-15'), $actual);
    }

    public function testInvalid(): void
    {
        $this->expectException(ValidationException::class);
        DateParameterConverter::convert('1992-04-31', '');
    }
}

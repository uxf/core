<?php

declare(strict_types=1);

namespace UXF\CoreTests\Integration\Request;

use PHPUnit\Framework\TestCase;
use UXF\Core\Exception\ValidationException;
use UXF\Core\RequestConverter\NationalIdentificationNumberCzeParameterConverter;
use UXF\Core\Type\NationalIdentificationNumberCze;

class NationalIdentificationNumberCzeParameterConverterTest extends TestCase
{
    public function testValid(): void
    {
        $actual = NationalIdentificationNumberCzeParameterConverter::convert('9302013545', '');
        self::assertEquals(NationalIdentificationNumberCze::of('930201/3545'), $actual);
    }

    public function testInvalid(): void
    {
        $this->expectException(ValidationException::class);
        NationalIdentificationNumberCzeParameterConverter::convert('1992-04-31', '');
    }
}

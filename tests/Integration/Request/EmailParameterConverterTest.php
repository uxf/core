<?php

declare(strict_types=1);

namespace UXF\CoreTests\Integration\Request;

use PHPUnit\Framework\TestCase;
use UXF\Core\Exception\ValidationException;
use UXF\Core\RequestConverter\EmailParameterConverter;
use UXF\Core\Type\Email;

class EmailParameterConverterTest extends TestCase
{
    public function testValid(): void
    {
        $actual = EmailParameterConverter::convert('test@test.com', '');
        self::assertEquals(Email::of('test@test.com'), $actual);
    }

    public function testInvalid(): void
    {
        $this->expectException(ValidationException::class);
        EmailParameterConverter::convert('test@test', '');
    }
}

<?php

declare(strict_types=1);

namespace UXF\CoreTests\Integration\Request;

use PHPUnit\Framework\TestCase;
use UXF\Core\Exception\ValidationException;
use UXF\Core\RequestConverter\DateTimeParameterConverter;

class DateTimeParameterConverterTest extends TestCase
{
    public function testValid(): void
    {
        $actual = DateTimeParameterConverter::convert('2022-01-08T15:28:08+01:00', '');
        self::assertSame('2022-01-08T15:28:08+01:00', $actual->__toString());
    }

    public function testInvalid(): void
    {
        $this->expectException(ValidationException::class);
        DateTimeParameterConverter::convert('2022-02-30', '');
    }
}

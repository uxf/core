<?php

declare(strict_types=1);

namespace UXF\CoreTests\Integration\Request;

use Nette\Utils\Json;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\Request;
use UXF\Core\Exception\ValidationException;
use UXF\Core\Http\Request\NotSet;
use UXF\Core\RequestConverter\BodyParameterConverter;
use UXF\Core\Type\BankAccountNumberCze;
use UXF\Core\Type\Currency;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Decimal;
use UXF\Core\Type\Email;
use UXF\Core\Type\Money;
use UXF\Core\Type\NationalIdentificationNumberCze;
use UXF\Core\Type\Phone;
use UXF\Core\Type\Time;
use UXF\Core\Type\Url;
use UXF\CoreTests\Integration\DbHelper;
use UXF\CoreTests\Project\Entity\Language;
use UXF\CoreTests\Project\Entity\State;
use UXF\CoreTests\Project\Http\TestPatchABody;
use UXF\CoreTests\Project\Http\TestPatchBBody;
use UXF\CoreTests\Project\Http\TestPhp8RequestBody;
use UXF\CoreTests\Project\Http\TestRequestBody;

class RequestBodyParameterConverterTest extends KernelTestCase
{
    private BodyParameterConverter $converter;

    protected function setUp(): void
    {
        self::bootKernel();
        DbHelper::initDatabase(self::getContainer());

        /** @var BodyParameterConverter $converter */
        $converter = self::getContainer()->get(BodyParameterConverter::class);
        $this->converter = $converter;
    }

    public function testValid(): void
    {
        $data = Json::encode([
            'int' => 2,
            'language' => 1,
            'state' => 'NEW',
        ]);
        $request = Request::create('', 'POST', [], [], [], [], $data);

        self::assertEquals(
            new TestRequestBody(
                language: Language::create(1, 'ěščřžýáíé'),
                state: State::NEW,
                int: 2,
            ),
            $this->converter->convert($request, TestRequestBody::class, false),
        );
    }

    public function testPatch(): void
    {
        $data = Json::encode([
            'string' => 'A',
            'array' => ['B', 'C'],
            'child' => [
                'string' => null,
            ],
            'children' => [[
                'string' => 'D',
            ], [
                'string' => null,
            ]],
        ]);
        $request = Request::create('', 'POST', [], [], [], [], $data);

        /** @var TestPatchABody $object */
        $object = $this->converter->convert($request, TestPatchABody::class, false);

        self::assertIsString($object->string);
        self::assertIsArray($object->array);
        self::assertInstanceOf(TestPatchBBody::class, $object->child);
        self::assertIsArray($object->children);

        self::assertInstanceOf(NotSet::class, $object->stringNotSet);
        self::assertInstanceOf(NotSet::class, $object->arrayNotSet);
        self::assertInstanceOf(NotSet::class, $object->childNotSet);
        self::assertInstanceOf(NotSet::class, $object->childrenNotSet);

        /** @var TestPatchBBody $child */
        $child = $object->child;
        self::assertNull($child->string);
        self::assertInstanceOf(NotSet::class, $child->stringNotSet);

        /** @var TestPatchBBody[] $children */
        $children = $object->children;
        [$children1, $children2] = $children;

        self::assertIsString($children1->string);
        self::assertInstanceOf(NotSet::class, $children1->stringNotSet);

        self::assertNull($children2->string);
        self::assertInstanceOf(NotSet::class, $children2->stringNotSet);
    }

    public function testArrayValid(): void
    {
        $data = Json::encode([[
            'int' => 2,
            'language' => 1,
        ]]);
        $request = Request::create('', 'POST', [], [], [], [], $data);

        self::assertEquals(
            [
                new TestRequestBody(
                    language: Language::create(1, 'ěščřžýáíé'),
                    int: 2,
                ),
            ],
            $this->converter->convert($request, TestRequestBody::class, true),
        );
    }

    public function testInvalid(): void
    {
        $data = Json::encode([
            'language' => 1,
        ]);
        $request = Request::create('', 'POST', [], [], [], [], $data);

        $this->expectException(ValidationException::class);
        $this->converter->convert($request, TestRequestBody::class, false);
    }

    public function testPhp8Valid(): void
    {
        $data = Json::encode([
            'emptyString' => 'ok',
            'emptyArray' => [1, 2],
            'language' => 1,
            'rawEmail' => 'info@uxf.cz',
            'int' => 1,
            'uuid' => '9e396235-1cca-4fe1-9d7c-94f848a186e7',
            'date' => '2020-01-02',
            'dateTime' => '2020-01-03',
            'time' => '11:12:13',
            'phone' => '777666777',
            'email' => 'info@uxf.cz',
            'ninCze' => '930201/3545',
            'banCze' => '123/0300',
            'money' => [
                'amount' => '-666.66',
                'currency' => 'CZK',
            ],
            'decimal' => '-456.78',
            'url' => 'https://uxf.cz',
        ]);
        $request = Request::create('', 'POST', [], [], [], [], $data);
        $object = $this->converter->convert($request, TestPhp8RequestBody::class, false);

        self::assertEquals(
            new TestPhp8RequestBody(
                emptyString: 'ok',
                emptyArray: [Language::create(1, 'ěščřžýáíé'), Language::create(2, 'ŽLUŤOUČKÝ')],
                language: Language::create(1, 'ěščřžýáíé'),
                rawEmail: 'info@uxf.cz',
                int: 1,
                uuid: Uuid::fromString('9e396235-1cca-4fe1-9d7c-94f848a186e7'),
                date: new Date('2020-01-02'),
                dateTime: new DateTime('2020-01-03'),
                time: new Time('11:12:13'),
                phone: Phone::of('+420777666777'),
                email: Email::of('info@uxf.cz'),
                ninCze: NationalIdentificationNumberCze::of('930201/3545'),
                banCze: BankAccountNumberCze::of('123/0300'),
                money: Money::of(-666.66, Currency::CZK),
                decimal: Decimal::of(-456.78),
                url: Url::of('https://uxf.cz'),
            ),
            $object,
        );

        self::assertInstanceOf(Language::class, $object->emptyArray[0]);
    }

    public function testPhp8Errors(): void
    {
        $data = Json::encode([
            'emptyString' => '',
            'emptyArray' => [],
            'language' => null,
            'rawEmail' => 'xx',
            'int' => 0,
            'uuid' => '9e396235-1cca-4fe1-9d7c-94f848a186e7',
            'date' => '2020-01-02',
            'dateTime' => '2022-01-08T15:28:08+01:00',
            'time' => '11:12:13',
            'phone' => '+420777666777',
            'email' => 'info@uxf.cz',
            'ninCze' => '930201/3545',
            'banCze' => '123/0300',
            'money' => [
                'amount' => '1',
                'currency' => 'CZK',
            ],
            'decimal' => '3.14',
            'url' => 'https://uxf.cz',
        ]);
        $request = Request::create('', 'POST', [], [], [], [], $data);

        try {
            $this->converter->convert($request, TestPhp8RequestBody::class, false);
        } catch (ValidationException $exception) {
            self::assertSame([
                [
                    'field' => 'emptyString',
                    'message' => 'This value should not be blank.',
                ],
                [
                    'field' => 'emptyArray',
                    'message' => 'This value should not be blank.',
                ],
                [
                    'field' => 'language',
                    'message' => 'This value should not be null.',
                ],
                [
                    'field' => 'rawEmail',
                    'message' => 'This value is not a valid email address.',
                ],
                [
                    'field' => 'int',
                    'message' => 'This value should be greater than 0.',
                ],
            ], $exception->getValidationErrors());
        }
    }
}

<?php

declare(strict_types=1);

namespace UXF\CoreTests\Integration;

use Doctrine\DBAL\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DbHelper
{
    public static function initDatabase(ContainerInterface $container): void
    {
        /** @var Connection $connection */
        $connection = $container->get(Connection::class);
        $connection->executeStatement('CREATE TABLE language (id int NOT NULL, name varchar NOT NULL)');
        $connection->executeStatement("INSERT INTO language (id, name) VALUES (1, 'ěščřžýáíé'), (2, 'ŽLUŤOUČKÝ');");

        $connection->executeStatement('CREATE TABLE hospital (id int NOT NULL, data varchar NOT NULL)');
        $connection->executeStatement('INSERT INTO hospital (id, data) VALUES 
            (1, \'[1]\'),
            (2, \'["2"]\'),
            (3, \'[true]\'),
            (4, \'{"name": "NAME"}\'),
            (5, \'[100, 101]\'),
            (6, \'[{"name": "XXX"}, {"name": "YYY"}]\')
        ');

        $connection->executeStatement('CREATE TABLE display (id int NOT NULL, data varchar NOT NULL)');
        $connection->executeStatement('INSERT INTO display (id, data) VALUES 
            (1, \'["HELLO", "WORLD"]\'),
            (5, \'["ok"]\')
        ');

        $connection->executeStatement('CREATE TABLE hello (id int NOT NULL, uuid varchar NOT NULL, created_at datetime NOT NULL, published_at date NOT NULL, open_at varchar NOT NULL, phone varchar NOT NULL, nin_cze varchar NOT NULL, ban_cze varchar NOT NULL)');
        $connection->executeStatement("INSERT INTO hello (id, uuid, created_at, published_at, open_at, phone, nin_cze, ban_cze) VALUES
            (1, '00000000-0000-0000-0000-000000000000', '2020-01-01 01:00:00', '2020-01-01', '10:00:00', '+420777666777', '930201/3545', '123/0300'),
            (2, '63845671-09bb-4e64-ba32-5a19f29254db', '2020-01-01 02:00:00', '2020-01-01', '10:00:00', '+420777666777', '930201/3545', '123/0300')
        ");

        $connection->executeStatement('CREATE TABLE item (id int NOT NULL)');
        $connection->executeStatement("INSERT INTO item (id) VALUES (1)");

        $connection->executeStatement('CREATE TABLE wizard (id int NOT NULL, type varchar NOT NULL, uuid varchar NOT NULL, name varchar NOT NULL, item_id int NOT NULL, enum varchar NOT NULL, enum_int varchar NOT NULL, hidden int NOT NULL)');
        $connection->executeStatement("INSERT INTO wizard (id, type, uuid, name, item_id, enum, enum_int, hidden) VALUES
            (1, 'w', '00000000-0000-0000-0000-000000000001', 'WOW!', 1, 'NEW', 1, 0)
        ");
    }
}

<?php

declare(strict_types=1);

use Ramsey\Uuid\Doctrine\UuidType;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use UXF\Core\Test\Client;
use UXF\CoreTests\Project\Controller\ModifiedResponseController;
use UXF\CoreTests\Project\FunnyService;
use UXF\CoreTests\Project\Http\LionParameterConverter;

return static function (ContainerConfigurator $configurator): void {
    $services = $configurator->services();
    $services->defaults()
        ->autowire()
        ->public();

    $configurator->extension('framework', [
        'test' => true,
        'validation' => [
            'email_validation_mode' => 'html5',
        ],
        'http_method_override' => false,
    ]);

    $configurator->extension('doctrine', [
        'dbal' => [
            'url' => 'sqlite:///:memory:',
            'types' => [
                'uuid' => UuidType::class,
            ],
        ],
        'orm' => [
            'naming_strategy' => 'doctrine.orm.naming_strategy.underscore_number_aware',
            'auto_mapping' => true,
            'report_fields_where_declared' => true,
            'enable_lazy_ghost_objects' => true,
            'mappings' => [
                'test' => [
                    'type' => 'attribute',
                    'dir' => __DIR__ . '/../Project/Entity',
                    'prefix' => 'UXF\CoreTests\Project\Entity',
                ],
            ],
        ],
    ]);

    $configurator->extension('security', [
        'firewalls' => [
            'secured' => [
                'stateless' => true,
                'pattern' => '^/error/firewall',
            ],
        ],
        'access_control' => [
            [
                'path' => '^/error/firewall',
                'roles' => 'ROLE_ROOT',
            ],
        ],
    ]);

    $services->set('test.client', Client::class);
    $services->set(ModifiedResponseController::class);
    $services->set(FunnyService::class)->public();
    $services->set(LionParameterConverter::class)->autoconfigure();
};

<?php

declare(strict_types=1);

namespace UXF\CoreTests;

use Doctrine\Bundle\DoctrineBundle\DoctrineBundle;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Bundle\MonologBundle\MonologBundle;
use Symfony\Bundle\SecurityBundle\SecurityBundle;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;
use UXF\Core\UXFCoreBundle;
use UXF\CoreTests\Project\Controller\BodyController;
use UXF\CoreTests\Project\Controller\ErrorController;
use UXF\CoreTests\Project\Controller\HeaderController;
use UXF\CoreTests\Project\Controller\ModifiedResponseController;
use UXF\CoreTests\Project\Controller\ParameterConverterController;
use UXF\Hydrator\UXFHydratorBundle;

class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    /**
     * @inheritDoc
     */
    public function registerBundles(): iterable
    {
        $bundles = [
            FrameworkBundle::class,
            MonologBundle::class,
            DoctrineBundle::class,
            UXFCoreBundle::class,
            UXFHydratorBundle::class,
            SecurityBundle::class,
        ];

        foreach ($bundles as $bundle) {
            yield new $bundle();
        }
    }

    protected function configureContainer(ContainerConfigurator $container): void
    {
        $container->import(__DIR__ . '/config/services.php');
    }

    protected function configureRoutes(RoutingConfigurator $routes): void
    {
        $routes->add('modify', '/modify-response')
            ->controller(ModifiedResponseController::class);

        $routes->add('converter', '/convert/{date}/{dateTime}/{time}/{email}/{phone}/{nin}/{enum}/{uuid}/{hello1}/{hello2}/{wizard}')
            ->controller(ParameterConverterController::class);

        $routes->add('body', '/body')
            ->controller(BodyController::class);

        $routes->add('header', '/header')
            ->controller(HeaderController::class);

        $routes->add('error', '/error/{type}')
            ->controller(ErrorController::class)
            ->methods(['GET']);
    }
}

<?php

declare(strict_types=1);

namespace UXF\Core\Utils;

use Symfony\Component\Finder\Finder;

final readonly class ClassFinder
{
    /**
     * @param string[] $dirs
     * @return class-string[]
     */
    public static function findAllClassNames(array $dirs): array
    {
        $finder = new Finder();

        $classNames = [];

        foreach ($finder->files()->in($dirs)->name('*.php') as $file) {
            $className = ClassNameHelper::resolve($file->getContents());
            if (SH::isNotEmpty($className)) {
                $classNames[] = $className;
            }
        }

        return $classNames;
    }
}

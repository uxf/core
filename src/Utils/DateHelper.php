<?php

declare(strict_types=1);

namespace UXF\Core\Utils;

use InvalidArgumentException;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;

final readonly class DateHelper
{
    public const array Days = [
        1 => 'Pondělí',
        2 => 'Úterý',
        3 => 'Středa',
        4 => 'Čtvrtek',
        5 => 'Pátek',
        6 => 'Sobota',
        7 => 'Neděle',
    ];

    public const array Months = [
        1 => 'Leden',
        2 => 'Únor',
        3 => 'Březen',
        4 => 'Duben',
        5 => 'Květen',
        6 => 'Červen',
        7 => 'Červenec',
        8 => 'Srpen',
        9 => 'Září',
        10 => 'Říjen',
        11 => 'Listopad',
        12 => 'Prosinec',
    ];

    public static function monthName(Date|DateTime|int $date, bool $lower = false): string
    {
        $date = is_int($date) ? $date : (int) $date->format('n');
        $output = self::Months[$date] ?? throw new InvalidArgumentException("Invalid month $date");
        return $lower ? mb_strtolower($output) : $output;
    }

    public static function dayName(Date|DateTime|int $date, bool $lower = false): string
    {
        $date = is_int($date) ? $date : (int) $date->format('N');
        $output = self::Days[$date] ?? throw new InvalidArgumentException("Invalid day $date");
        return $lower ? mb_strtolower($output) : $output;
    }
}

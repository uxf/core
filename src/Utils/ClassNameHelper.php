<?php

declare(strict_types=1);

namespace UXF\Core\Utils;

use Nette\Utils\Strings;

final readonly class ClassNameHelper
{
    /**
     * @return class-string|null
     */
    public static function resolve(string $content): ?string
    {
        if (preg_match('/^namespace (.+);/m', $content, $m1) !== 1) {
            return null;
        }

        if (
            preg_match('/^enum ([a-zA-Z0-9]+).*$/m', $content, $m2) === 1 ||
            preg_match('/^(?:readonly |final |abstract |)*class ([a-zA-Z0-9]+).*$/m', $content, $m2) === 1
        ) {
            $className = $m1[1] . '\\' . $m2[1];
            return class_exists($className) ? $className : null;
        }

        return null;
    }

    /**
     * @param object|class-string $entity
     * @return class-string
     */
    public static function normalizeClassName(object|string $entity): string
    {
        $entity = is_string($entity) ? $entity : $entity::class;
        return str_replace('Proxies\\__CG__\\', '', $entity); // @phpstan-ignore-line
    }

    public static function shortName(string $className): string
    {
        return Strings::replace($className, '/.*\\\\/');
    }
}

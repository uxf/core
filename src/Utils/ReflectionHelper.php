<?php

declare(strict_types=1);

namespace UXF\Core\Utils;

use ReflectionException;
use ReflectionProperty;

final readonly class ReflectionHelper
{
    public static function findReflectionProperty(object|string $class, string $property): ?ReflectionProperty
    {
        try {
            return new ReflectionProperty($class, $property);
        } catch (ReflectionException) {
            $parentClass = get_parent_class($class);
            if ($parentClass !== false) {
                return self::findReflectionProperty($parentClass, $property);
            }

            return null;
        }
    }
}

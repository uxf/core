<?php

declare(strict_types=1);

namespace UXF\Core\ValueResolver;

use BackedEnum;
use LogicException;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use UXF\Core\Attribute\Entity;
use UXF\Core\Attribute\FromBody;
use UXF\Core\Attribute\FromHeader;
use UXF\Core\Attribute\FromQuery;
use UXF\Core\RequestConverter\BodyParameterConverter;
use UXF\Core\RequestConverter\DateParameterConverter;
use UXF\Core\RequestConverter\DateTimeParameterConverter;
use UXF\Core\RequestConverter\DecimalParameterConverter;
use UXF\Core\RequestConverter\EmailParameterConverter;
use UXF\Core\RequestConverter\EntityParameterConverter;
use UXF\Core\RequestConverter\EnumParameterConverter;
use UXF\Core\RequestConverter\HeaderParameterConverter;
use UXF\Core\RequestConverter\NationalIdentificationNumberCzeParameterConverter;
use UXF\Core\RequestConverter\PhoneParameterConverter;
use UXF\Core\RequestConverter\QueryParameterConverter;
use UXF\Core\RequestConverter\TimeParameterConverter;
use UXF\Core\RequestConverter\UrlParameterConverter;
use UXF\Core\RequestConverter\UuidParameterConverter;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Decimal;
use UXF\Core\Type\Email;
use UXF\Core\Type\NationalIdentificationNumberCze;
use UXF\Core\Type\Phone;
use UXF\Core\Type\Time;
use UXF\Core\Type\Url;
use UXF\Core\Utils\ReflectionHelper;

final readonly class CoreValueResolver implements ValueResolverInterface
{
    public function __construct(
        private BodyParameterConverter $bodyParameterConverter,
        private ?EntityParameterConverter $entityParameterConverter,
        private HeaderParameterConverter $headerParameterConverter,
        private QueryParameterConverter $queryParameterConverter,
    ) {
    }

    /**
     * @return array<mixed>
     */
    public function resolve(Request $request, ArgumentMetadata $argument): array
    {
        $name = $argument->getName();
        /** @var literal-string $typeName */
        $typeName = $argument->getType();

        // body + query from attribute
        $attribute = $argument->getAttributes()[0] ?? null;
        if ($attribute !== null) {
            if ($attribute instanceof FromBody) {
                if ($attribute->arrayClassName === null) {
                    $isArray = false;
                } else {
                    $typeName = $attribute->arrayClassName;
                    $isArray = true;
                }

                assert(class_exists($typeName) || interface_exists($typeName) || $typeName === 'mixed');
                return [$this->bodyParameterConverter->convert($request, $typeName, $isArray)];
            }

            if ($attribute instanceof FromQuery) {
                assert(class_exists($typeName) || interface_exists($typeName));
                return [$this->queryParameterConverter->convert($request, $typeName)];
            }

            if ($attribute instanceof FromHeader) {
                assert(class_exists($typeName) || interface_exists($typeName));
                return [$this->headerParameterConverter->convert($request, $typeName)];
            }

            if ($this->entityParameterConverter !== null && $attribute instanceof Entity) {
                $property = $attribute->property;
                $value = $request->attributes->get($name);
                assert(class_exists($typeName));
                if (ReflectionHelper::findReflectionProperty($typeName, $property) === null) {
                    throw new LogicException("Invalid property $typeName::\$$property");
                }
                return [$this->entityParameterConverter->convert($value, $typeName, $property)];
            }
        }

        // path - from class
        $value = $request->attributes->get($name);
        if ($value === null || (!class_exists($typeName) && !interface_exists($typeName))) {
            return [];
        }

        $result = match ($typeName) {
            Date::class => [DateParameterConverter::convert($value, $name)],
            DateTime::class => [DateTimeParameterConverter::convert($value, $name)],
            Email::class => [EmailParameterConverter::convert($value, $name)],
            NationalIdentificationNumberCze::class => [NationalIdentificationNumberCzeParameterConverter::convert($value, $name)],
            Phone::class => [PhoneParameterConverter::convert($value, $name)],
            Time::class => [TimeParameterConverter::convert($value, $name)],
            Decimal::class => [DecimalParameterConverter::convert($value, $name)],
            Url::class => [UrlParameterConverter::convert($value, $name)],
            UuidInterface::class => [UuidParameterConverter::convert($value, $name)],
            default => null,
        };
        if ($result !== null) {
            return $result;
        }

        if (is_a($typeName, BackedEnum::class, true)) {
            return [EnumParameterConverter::convert($value, $typeName, $name)];
        }

        if ($this->entityParameterConverter?->support($typeName) === true) {
            return [$this->entityParameterConverter->convert($value, $typeName, null)];
        }

        return [];
    }
}

<?php

declare(strict_types=1);

namespace UXF\Core;

use Doctrine\DBAL\Types\Type;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;
use UXF\Core\DependencyInjection\EntityHydratorCompilerPass;
use UXF\Core\Doctrine\Func\Cast;
use UXF\Core\Doctrine\Func\Date;
use UXF\Core\Doctrine\Func\JsonbContains;
use UXF\Core\Doctrine\Func\JsonbContainsSubstring;
use UXF\Core\Doctrine\Func\Time;
use UXF\Core\Doctrine\Func\Unaccent;
use UXF\Core\Doctrine\Middleware\DoctrineSQLiteMiddleware;
use UXF\Core\Doctrine\Type\DoctrineBankAccountNumberCzeType;
use UXF\Core\Doctrine\Type\DoctrineDateTimeType;
use UXF\Core\Doctrine\Type\DoctrineDateType;
use UXF\Core\Doctrine\Type\DoctrineDecimalType;
use UXF\Core\Doctrine\Type\DoctrineEmailType;
use UXF\Core\Doctrine\Type\DoctrineJsonType;
use UXF\Core\Doctrine\Type\DoctrineNationalIdentificationNumberCzeType;
use UXF\Core\Doctrine\Type\DoctrinePhoneType;
use UXF\Core\Doctrine\Type\DoctrineSearchType;
use UXF\Core\Doctrine\Type\DoctrineTimeType;
use UXF\Core\Doctrine\Type\DoctrineUrlType;
use UXF\Core\Http\Request\NotSet;
use UXF\Core\Hydrator\HydratorTranslator;
use UXF\Core\Test\SqliteDefaultQuoteStrategy;

final class UXFCoreBundle extends AbstractBundle
{
    /**
     * @param array<mixed> $config
     */
    public function loadExtension(array $config, ContainerConfigurator $container, ContainerBuilder $builder): void
    {
        $container->import(__DIR__ . '/../config/services.php');
    }

    public function prependExtension(ContainerConfigurator $container, ContainerBuilder $builder): void
    {
        $container->extension('uxf_hydrator', [
            'translator' => HydratorTranslator::class,
            'ignored_types' => [NotSet::class],
            'default_options' => [
                'allow_fallback' => true,
                'allow_trim_string' => true,
            ],
        ]);

        $bundles = $builder->getParameter('kernel.bundles');
        if (isset($bundles['DoctrineBundle'])) {
            $container->extension('doctrine', [
                'orm' => [
                    'dql' => [
                        'string_functions' => [
                            'CAST' => Cast::class,
                            'DATE' => Date::class,
                            'TIME' => Time::class,
                            'UNACCENT' => Unaccent::class,
                            'JSONB_CONTAINS' => JsonbContains::class,
                            'JSONB_CONTAINS_SUBSTRING' => JsonbContainsSubstring::class,
                        ],
                    ],
                ],
            ]);

            if ($container->env() === 'test') {
                $container->extension('doctrine', [
                    'orm' => [
                        'quote_strategy' => 'doctrine.orm.quote_strategy.sqlite',
                    ],
                ]);

                $services = $container->services();
                $services->set('doctrine.orm.quote_strategy.sqlite', SqliteDefaultQuoteStrategy::class);
                $services->set(DoctrineSQLiteMiddleware::class)->autoconfigure();
            }
        }
    }

    public function build(ContainerBuilder $container): void
    {
        parent::build($container);

        $container->addCompilerPass(new EntityHydratorCompilerPass());
    }

    public function boot(): void
    {
        // dbal is optional
        if (class_exists(Type::class)) {
            DoctrineJsonType::register();
            DoctrineSearchType::register();
            DoctrineDateTimeType::register();
            DoctrineDateType::register();
            DoctrineDecimalType::register();
            DoctrineUrlType::register();
            DoctrineTimeType::register();
            DoctrinePhoneType::register();
            DoctrineEmailType::register();
            DoctrineNationalIdentificationNumberCzeType::register();
            DoctrineBankAccountNumberCzeType::register();
        }
    }
}

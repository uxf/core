<?php

declare(strict_types=1);

namespace UXF\Core\Exception;

use Psr\Log\LogLevel;
use RuntimeException;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use UXF\Hydrator\Exception\HydratorCollectionException;

class ValidationException extends RuntimeException implements CoreException
{
    /**
     * @param array<array{message: string, field: string}> $validationErrors
     */
    public function __construct(
        private readonly array $validationErrors,
        string $message = 'INVALID INPUT',
    ) {
        parent::__construct($message);
    }

    /**
     * @param array<string, array<string>> $errors
     */
    public static function fromHydratorErrors(array $errors, string $prefix = ''): self
    {
        $validationErrors = [];
        foreach ($errors as $path => $messages) {
            foreach ($messages as $message) {
                $validationErrors[] = [
                    'field' => "$prefix$path",
                    'message' => $message,
                ];
            }
        }

        return new self($validationErrors);
    }

    public static function fromHydratorCollectionException(HydratorCollectionException $exception): self
    {
        $validationErrors = [];
        $isList = array_is_list($exception->exceptions);
        foreach ($exception->exceptions as $topPath => $e) {
            foreach ($e->errors as $innerPath => $messages) {
                $path = $isList ? "$topPath:$innerPath" : "$topPath.$innerPath";
                foreach ($messages as $message) {
                    $validationErrors[] = [
                        'field' => $path,
                        'message' => $message,
                    ];
                }
            }
        }

        return new self($validationErrors);
    }

    public static function fromValidator(ValidatorInterface $validator, mixed $result): ?self
    {
        $validationErrors = $validator->validate($result);
        if ($validationErrors->count() === 0) {
            return null;
        }

        $errors = [];
        /** @var ConstraintViolation $error */
        foreach ($validationErrors as $error) {
            $errors[] = [
                'field' => $error->getPropertyPath(),
                'message' => (string) $error->getMessage(),
            ];
        }

        return new self($errors);
    }

    public function getHttpStatusCode(): int
    {
        return 400;
    }

    public function getErrorCode(): string
    {
        return 'BAD_USER_INPUT';
    }

    public function getLevel(): string
    {
        return LogLevel::INFO;
    }

    /**
     * @inheritDoc
     */
    public function getValidationErrors(): ?array
    {
        return $this->validationErrors;
    }
}

<?php

declare(strict_types=1);

namespace UXF\Core\Exception;

use RuntimeException;
use Throwable;

final class MoneyException extends RuntimeException
{
    public function __construct(string $message, ?Throwable $previous = null)
    {
        parent::__construct($message, previous: $previous);
    }
}

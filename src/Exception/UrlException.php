<?php

declare(strict_types=1);

namespace UXF\Core\Exception;

use RuntimeException;

final class UrlException extends RuntimeException
{
    public function __construct(string $message)
    {
        parent::__construct($message);
    }
}

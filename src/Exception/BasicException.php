<?php

declare(strict_types=1);

namespace UXF\Core\Exception;

use Psr\Log\LogLevel;
use RuntimeException;

class BasicException extends RuntimeException implements CoreException
{
    /**
     * @param LogLevel::* $level
     */
    public function __construct(
        string $message,
        private readonly int $httpStatusCode = 400,
        private readonly string $errorCode = 'BAD_REQUEST',
        private readonly string $level = LogLevel::INFO,
    ) {
        parent::__construct($message);
    }

    /**
     * @param LogLevel::* $level
     */
    public static function badRequest(
        string $message = 'BAD REQUEST',
        string $errorCode = 'BAD_REQUEST',
        string $level = LogLevel::INFO,
    ): self {
        return new self($message, 400, $errorCode, $level);
    }

    /**
     * @param LogLevel::* $level
     */
    public static function unauthorized(
        string $message = 'UNAUTHORIZED',
        string $errorCode = 'UNAUTHORIZED',
        string $level = LogLevel::INFO,
    ): self {
        return new self($message, 401, $errorCode, $level);
    }

    /**
     * @param LogLevel::* $level
     */
    public static function forbidden(
        string $message = 'FORBIDDEN',
        string $errorCode = 'FORBIDDEN',
        string $level = LogLevel::INFO,
    ): self {
        return new self($message, 403, $errorCode, $level);
    }

    /**
     * @param LogLevel::* $level
     */
    public static function notFound(
        string $message = 'NOT FOUND',
        string $errorCode = 'NOT_FOUND',
        string $level = LogLevel::INFO,
    ): self {
        return new self($message, 404, $errorCode, $level);
    }

    public function getHttpStatusCode(): int
    {
        return $this->httpStatusCode;
    }

    public function getErrorCode(): string
    {
        return $this->errorCode;
    }

    public function getLevel(): string
    {
        return $this->level;
    }

    /**
     * @inheritDoc
     */
    public function getValidationErrors(): ?array
    {
        return null;
    }
}

<?php

declare(strict_types=1);

namespace UXF\Core\EventSubscriber;

use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Throwable;
use UXF\Core\Exception\CoreException;
use UXF\Core\Http\Response\ErrorResponse;

final readonly class ExceptionSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private string $env,
        private bool $debug,
        private LoggerInterface $logger,
    ) {
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            ExceptionEvent::class => [
                ['processException', -16], // after firewall listener (p: 1) and kernel error log listener with disabled logging
            ],
        ];
    }

    public function processException(ExceptionEvent $event): void
    {
        $response = $this->resolveResponse($event->getThrowable());
        if ($response !== null) {
            $event->setResponse($response);
        }
    }

    private function resolveResponse(Throwable $exception): ?Response
    {
        switch (true) {
            case $exception instanceof CoreException:
                $this->logger->log($exception->getLevel(), $exception->getMessage(), [
                    'exception' => $exception,
                ]);
                return ErrorResponse::fromCoreException($exception);
            case $exception instanceof HttpExceptionInterface:
                return ErrorResponse::fromHttpException($exception);
            default:
                $this->logger->critical($exception->getMessage(), [
                    'exception' => $exception,
                ]);

                if ($this->env === 'test') {
                    return new Response((string) $exception, 500);
                }

                return $this->debug ? null : ErrorResponse::serverError();
        }
    }
}

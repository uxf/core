<?php

declare(strict_types=1);

namespace UXF\Core\Http;

use Symfony\Component\HttpFoundation\Response;

/**
 * @template T
 */
interface ResponseModifier
{
    /**
     * @phpstan-param T $data
     */
    public static function modifyResponse(Response $response, mixed $data): void;
}

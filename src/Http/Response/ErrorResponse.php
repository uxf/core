<?php

declare(strict_types=1);

namespace UXF\Core\Http\Response;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use UXF\Core\Exception\CoreException;

final class ErrorResponse extends JsonResponse
{
    /**
     * @param array<array{message: string, field: string}> $validationErrors
     */
    public function __construct(int $status, string $errorCode, string $message, array $validationErrors)
    {
        parent::__construct([], $status, [
            'Content-Type' => 'application/json; charset=utf-8',
        ]);
        $this->setEncodingOptions(JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        $this->setData([
            'error' => [
                'code' => $errorCode,
                'message' => $message,
            ],
            'validationErrors' => $validationErrors,
        ]);
    }

    public static function badRequest(string $errorCode = 'BAD_REQUEST', string $message = 'BAD REQUEST'): self
    {
        return new self(400, $errorCode, $message, []);
    }

    public static function unauthorized(string $errorCode = 'UNAUTHORIZED', string $message = 'UNAUTHORIZED'): self
    {
        return new self(401, $errorCode, $message, []);
    }

    public static function forbidden(string $errorCode = 'FORBIDDEN', string $message = 'FORBIDDEN'): self
    {
        return new self(403, $errorCode, $message, []);
    }

    public static function notFound(string $errorCode = 'NOT_FOUND', string $message = 'NOT FOUND'): self
    {
        return new self(404, $errorCode, $message, []);
    }

    public static function serverError(string $errorCode = 'SERVER_ERROR', string $message = 'SERVER ERROR'): self
    {
        return new self(500, $errorCode, $message, []);
    }

    public static function fromHttpException(HttpExceptionInterface $exception): self
    {
        $message = match ($exception->getStatusCode()) {
            401 => 'UNAUTHORIZED',
            403 => 'FORBIDDEN',
            404 => 'NOT_FOUND',
            405 => 'BAD_REQUEST',
            default => $exception->getMessage(),
        };

        return new self($exception->getStatusCode(), $message, $message, []);
    }

    public static function fromCoreException(CoreException $exception): self
    {
        return new self($exception->getHttpStatusCode(), $exception->getErrorCode(), $exception->getMessage(), $exception->getValidationErrors() ?? []);
    }
}

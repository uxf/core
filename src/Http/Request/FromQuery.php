<?php

declare(strict_types=1);

namespace UXF\Core\Http\Request;

use Attribute;
use UXF\Core\Attribute\FromQuery as CoreFromQuery;

/**
 * @deprecated Please use UXF\Core\Attribute\FromQuery
 */
#[Attribute(Attribute::TARGET_PARAMETER)]
final readonly class FromQuery extends CoreFromQuery
{
}

<?php

declare(strict_types=1);

namespace UXF\Core\Test;

use Nette\Utils\Strings;
use function Safe\json_decode;

final readonly class SqlitePlatform
{
    public static function unaccent(?string $value): ?string
    {
        return $value !== null ? Strings::lower(Strings::toAscii($value)) : $value;
    }

    public static function jsonbContains(?string $a, ?string $b): int
    {
        if ($a === null || $b === null) {
            return 0;
        }

        $items = json_decode($a, true);
        $values = json_decode($b, true);
        $isList = array_is_list($values);

        foreach ($values as $searchName => $searchValue) {
            if (
                ($isList && !in_array($searchValue, $items, true)) ||
                (!$isList && (!isset($items[$searchName]) || $items[$searchName] !== $searchValue))
            ) {
                return 0;
            }
        }

        return 1;
    }

    public static function jsonbContainsSubstring(?string $a, ?string $value): int
    {
        if ($a === null || $value === null) {
            return 0;
        }

        if ($value === '%%') {
            return 1;
        }

        $items = json_decode($a, true);

        $startsWith = mb_substr($value, -1) === '%';
        $endsWith = mb_strpos($value, '%') === 0;
        $value = trim($value, '%');

        if ($startsWith && $endsWith) {
            foreach ($items as $item) {
                if (str_contains($item, $value)) {
                    return 1;
                }
            }
        }

        if ($startsWith) {
            foreach ($items as $item) {
                if (str_starts_with($item, $value)) {
                    return 1;
                }
            }
        }

        if ($endsWith) {
            foreach ($items as $item) {
                if (str_ends_with($item, $value)) {
                    return 1;
                }
            }
        }

        return in_array($value, $items, true) ? 1 : 0;
    }
}

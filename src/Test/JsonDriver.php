<?php

declare(strict_types=1);

namespace UXF\Core\Test;

use Nette\Utils\Json;
use PHPUnit\Framework\Assert;
use Spatie\Snapshots\Driver;
use Spatie\Snapshots\Exceptions\CantBeSerialized;

final readonly class JsonDriver implements Driver
{
    public function serialize(mixed $data): string
    {
        if (is_string($data)) {
            $data = Json::decode($data);
        }

        if (is_resource($data)) {
            throw new CantBeSerialized('Resources can not be serialized to json');
        }

        return self::encode($data);
    }

    public function extension(): string
    {
        return 'json';
    }

    public function match(mixed $expected, mixed $actual): void
    {
        if (is_array($actual) || is_object($actual)) {
            $actual = self::encode($actual);
        }

        Assert::assertJsonStringEqualsJsonString($expected, $actual);
    }

    /**
     * @param array<mixed> $data
     */
    private static function encode(array|object|null $data): string
    {
        return Json::encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES) . "\n";
    }
}

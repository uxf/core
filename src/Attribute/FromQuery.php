<?php

declare(strict_types=1);

namespace UXF\Core\Attribute;

use Attribute;

/**
 * @final
 */
#[Attribute(Attribute::TARGET_PARAMETER)]
readonly class FromQuery
{
}

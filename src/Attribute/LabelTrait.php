<?php

declare(strict_types=1);

namespace UXF\Core\Attribute;

use ReflectionEnumBackedCase;

trait LabelTrait
{
    public function getLabel(): string
    {
        return (new ReflectionEnumBackedCase($this, $this->name))->getAttributes(Label::class)[0]->newInstance()->label;
    }
}

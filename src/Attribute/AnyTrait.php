<?php

declare(strict_types=1);

namespace UXF\Core\Attribute;

trait AnyTrait
{
    public function any(self ...$others): bool
    {
        return in_array($this, $others, true);
    }
}

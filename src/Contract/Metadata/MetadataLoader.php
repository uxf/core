<?php

declare(strict_types=1);

namespace UXF\Core\Contract\Metadata;

/**
 * @deprecated
 */
interface MetadataLoader
{
    /**
     * @return array<string, Metadata>
     */
    public function getMetadata(): array;
}

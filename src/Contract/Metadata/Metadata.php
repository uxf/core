<?php

declare(strict_types=1);

namespace UXF\Core\Contract\Metadata;

final readonly class Metadata
{
    /**
     * @param string[] $actions
     */
    public function __construct(
        public string $title,
        public string $entityAlias,
        public array $actions,
    ) {
    }
}

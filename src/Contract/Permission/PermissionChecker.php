<?php

declare(strict_types=1);

namespace UXF\Core\Contract\Permission;

interface PermissionChecker
{
    public function isAllowed(string $resourceType, string $resourceName): bool;
}

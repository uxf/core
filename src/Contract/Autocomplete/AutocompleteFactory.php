<?php

declare(strict_types=1);

namespace UXF\Core\Contract\Autocomplete;

interface AutocompleteFactory
{
    /**
     * @param array<mixed> $options
     */
    public function createAutocomplete(string $name, array $options = []): Autocomplete;
}

<?php

declare(strict_types=1);

namespace UXF\Core\Contract\Autocomplete;

interface Autocomplete
{
    /**
     * @return AutocompleteResult[]
     */
    public function search(string $term, int $limit): array;
}

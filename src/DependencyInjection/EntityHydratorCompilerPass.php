<?php

declare(strict_types=1);

namespace UXF\Core\DependencyInjection;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use UXF\Core\Hydrator\ParameterGenerator\DoctrineParameterGenerator;
use UXF\Core\RequestConverter\EntityParameterConverter;

final readonly class EntityHydratorCompilerPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container): void
    {
        // doctrine is optional
        if (!$container->has(EntityManagerInterface::class)) {
            $container->removeDefinition(EntityParameterConverter::class);
            $container->removeDefinition(DoctrineParameterGenerator::class);
        }
    }
}

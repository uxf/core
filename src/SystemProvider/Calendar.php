<?php

declare(strict_types=1);

namespace UXF\Core\SystemProvider;

use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;

final class Calendar
{
    public static ?Date $frozenDate = null;

    public static function today(): Date
    {
        return self::$frozenDate ?? Date::createFromDateTime(new DateTime());
    }
}

<?php

declare(strict_types=1);

namespace UXF\Core\SystemProvider;

use UXF\Core\Type\DateTime;

final class Clock
{
    public static ?DateTime $frozenTime = null;

    public static function now(): DateTime
    {
        return self::$frozenTime ?? new DateTime();
    }
}

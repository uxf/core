<?php

declare(strict_types=1);

namespace UXF\Core\Doctrine\Func;

use Doctrine\DBAL\Platforms\PostgreSQLPlatform;
use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\AST\Node;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;
use Doctrine\ORM\Query\TokenType;

// JSONB_CONTAINS(root.items, :value) = TRUE
// value = json_encode([1]) OR json_encode(['A']) OR json_encode(['name' => 'Peter'])
final class JsonbContains extends FunctionNode
{
    public Node $column; // @phpstan-ignore-line
    public Node $value; // @phpstan-ignore-line

    public function getSql(SqlWalker $sqlWalker): string
    {
        $column = $this->column->dispatch($sqlWalker);
        $value = $this->value->dispatch($sqlWalker);

        if ($sqlWalker->getConnection()->getDatabasePlatform() instanceof PostgreSQLPlatform) {
            // SELECT '[1, 2, 3]'::jsonb @> '[1]'::jsonb AS contains_value;
            return "$column @> $value::JSONB";
        }

        return "jsonb_contains($column, $value)";
    }

    public function parse(Parser $parser): void
    {
        $parser->match(TokenType::T_IDENTIFIER);
        $parser->match(TokenType::T_OPEN_PARENTHESIS);
        $this->column = $parser->StringPrimary();
        $parser->match(TokenType::T_COMMA);
        $this->value = $parser->StringPrimary();
        $parser->match(TokenType::T_CLOSE_PARENTHESIS);
    }
}

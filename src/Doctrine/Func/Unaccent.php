<?php

declare(strict_types=1);

namespace UXF\Core\Doctrine\Func;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\AST\Node;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;
use Doctrine\ORM\Query\TokenType;

final class Unaccent extends FunctionNode
{
    private Node $string;  // @phpstan-ignore-line

    public function getSql(SqlWalker $sqlWalker): string
    {
        return 'LOWER(UNACCENT(' . $this->string->dispatch($sqlWalker) . '))';
    }

    public function parse(Parser $parser): void
    {
        $parser->match(TokenType::T_IDENTIFIER);
        $parser->match(TokenType::T_OPEN_PARENTHESIS);
        $this->string = $parser->StringPrimary();
        $parser->match(TokenType::T_CLOSE_PARENTHESIS);
    }
}

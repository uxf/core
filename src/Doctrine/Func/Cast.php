<?php

declare(strict_types=1);

namespace UXF\Core\Doctrine\Func;

use Doctrine\ORM\Query\AST\ArithmeticExpression;
use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;
use Doctrine\ORM\Query\TokenType;
use RuntimeException;

final class Cast extends FunctionNode
{
    public ?ArithmeticExpression $columnExpression = null;

    public ?string $type = null;

    public function getSql(SqlWalker $sqlWalker): string
    {
        if ($this->columnExpression === null) {
            throw new RuntimeException();
        }

        // CAST("id" AS text)
        return 'CAST(' .
            $this->columnExpression->dispatch($sqlWalker) .
            ' AS ' .
            $this->type .
            ')';
    }

    public function parse(Parser $parser): void
    {
        $parser->match(TokenType::T_IDENTIFIER);
        $parser->match(TokenType::T_OPEN_PARENTHESIS);
        $this->columnExpression = $parser->ArithmeticExpression();
        $parser->match(TokenType::T_AS);
        $parser->match(TokenType::T_IDENTIFIER);
        $this->type = $parser->getLexer()->token->value ?? '';
        $parser->match(TokenType::T_CLOSE_PARENTHESIS);
    }
}

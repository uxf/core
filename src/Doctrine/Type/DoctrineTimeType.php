<?php

declare(strict_types=1);

namespace UXF\Core\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\Type;
use UXF\Core\Type\Time;

final class DoctrineTimeType extends Type
{
    public static function register(): void
    {
        if (self::hasType(Time::class)) {
            return;
        }

        self::addType(Time::class, self::class);
    }

    public function getName(): string
    {
        return Time::class;
    }

    /**
     * @param array<mixed> $column
     */
    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        return $platform->getTimeTypeDeclarationSQL($column);
    }

    public function convertToDatabaseValue(mixed $value, AbstractPlatform $platform): ?string
    {
        if ($value === null) {
            return null;
        }

        if ($value instanceof Time) {
            return (string) $value;
        }

        throw new ConversionException();
    }

    public function convertToPHPValue(mixed $value, AbstractPlatform $platform): mixed
    {
        if ($value === null || $value instanceof Time) {
            return $value;
        }

        return new Time($value);
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }
}

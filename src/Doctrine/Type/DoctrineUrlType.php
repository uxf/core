<?php

declare(strict_types=1);

namespace UXF\Core\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;
use UXF\Core\Type\Url;

final class DoctrineUrlType extends StringType
{
    public static function register(): void
    {
        if (self::hasType(Url::class)) {
            return;
        }

        self::addType(Url::class, self::class);
    }

    public function convertToPHPValue(mixed $value, AbstractPlatform $platform): ?Url
    {
        if ($value === null) {
            return null;
        }

        if ($value instanceof Url) {
            return $value;
        }

        return Url::createFromTrustedSource($value);
    }

    public function convertToDatabaseValue(mixed $value, AbstractPlatform $platform): ?string
    {
        if ($value instanceof Url) {
            return $value->toString();
        }

        return $value;
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    public function getName(): string
    {
        return Url::class;
    }
}

<?php

declare(strict_types=1);

namespace UXF\Core\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\StringType;
use UXF\Core\Type\NationalIdentificationNumberCze;

final class DoctrineNationalIdentificationNumberCzeType extends StringType
{
    public static function register(): void
    {
        if (self::hasType(NationalIdentificationNumberCze::class)) {
            return;
        }

        self::addType(NationalIdentificationNumberCze::class, self::class);
    }

    /**
     * @inheritDoc
     */
    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        $column['length'] ??= 11;
        return parent::getSQLDeclaration($column, $platform);
    }

    public function convertToDatabaseValue(mixed $value, AbstractPlatform $platform): ?string
    {
        if ($value === null) {
            return null;
        }

        if ($value instanceof NationalIdentificationNumberCze) {
            return (string) $value;
        }

        throw new ConversionException();
    }

    public function convertToPHPValue(mixed $value, AbstractPlatform $platform): ?NationalIdentificationNumberCze
    {
        if ($value === null || $value instanceof NationalIdentificationNumberCze) {
            return $value;
        }

        return NationalIdentificationNumberCze::createFromTrustedSource($value);
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    public function getName(): string
    {
        return NationalIdentificationNumberCze::class;
    }
}

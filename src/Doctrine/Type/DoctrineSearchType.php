<?php

declare(strict_types=1);

namespace UXF\Core\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\TextType;

final class DoctrineSearchType extends TextType
{
    public static function register(): void
    {
        if (self::hasType('uxf_search')) {
            return;
        }

        self::addType('uxf_search', self::class);
    }

    /**
     * @param string|mixed $sqlExpr - BC with DBAL 3
     */
    public function convertToDatabaseValueSQL(mixed $sqlExpr, AbstractPlatform $platform): string
    {
        return sprintf("LOWER(UNACCENT(%s))", $sqlExpr);
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    public function getName(): string
    {
        return 'uxf_search';
    }
}

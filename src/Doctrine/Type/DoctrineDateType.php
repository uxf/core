<?php

declare(strict_types=1);

namespace UXF\Core\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\Type;
use UXF\Core\Type\Date;

final class DoctrineDateType extends Type
{
    public static function register(): void
    {
        if (self::hasType(Date::class)) {
            return;
        }

        self::addType(Date::class, self::class);
    }

    public function getName(): string
    {
        return Date::class;
    }

    /**
     * @param array<mixed> $column
     */
    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        return $platform->getDateTypeDeclarationSQL($column);
    }

    public function convertToDatabaseValue(mixed $value, AbstractPlatform $platform): ?string
    {
        if ($value === null) {
            return null;
        }

        if ($value instanceof Date) {
            return (string) $value;
        }

        throw new ConversionException();
    }

    public function convertToPHPValue(mixed $value, AbstractPlatform $platform): mixed
    {
        if ($value === null || $value instanceof Date) {
            return $value;
        }

        return new Date($value);
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }
}

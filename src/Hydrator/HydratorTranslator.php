<?php

declare(strict_types=1);

namespace UXF\Core\Hydrator;

use UXF\Hydrator\Translator\DefaultTranslator;
use UXF\Hydrator\Translator\ErrorInfo;
use UXF\Hydrator\Translator\Translator;

final readonly class HydratorTranslator implements Translator
{
    private const array Translations = [
        'date.invalid_format' => 'Invalid date format',
        'date_time.invalid_format' => 'Invalid date time format',
        'decimal.invalid_format' => 'Invalid number format',
        'url.invalid_format' => 'Invalid url format',
        'time.invalid_format' => 'Invalid time format',
        'phone.invalid_format' => 'Invalid phone format',
        'email.invalid_format' => 'Invalid email format',
        'nin_cze.invalid_format' => 'Invalid national identification number',
        'ban_cze.invalid_format' => 'Invalid bank account number',
        'doctrine.not_found' => 'Not found',
        'uuid.invalid_format' => 'Invalid uuid format',
        'money.invalid_amount_format' => 'Invalid money amount format',
        'money.invalid_currency_format' => 'Invalid money currency format',
    ];

    public function __construct(private DefaultTranslator $fallback = new DefaultTranslator())
    {
    }

    public function trans(string $key, ?ErrorInfo $info = null): string
    {
        if (array_key_exists($key, self::Translations)) {
            return self::Translations[$key] . DefaultTranslator::resolveMessage($info);
        }

        return $this->fallback->trans($key, $info);
    }
}

<?php

declare(strict_types=1);

namespace UXF\Core\Hydrator\ParameterGenerator;

use UXF\Core\Type\Currency;
use UXF\Core\Type\Money;
use UXF\Hydrator\Inspector\ParameterDefinition;
use UXF\Hydrator\Options;
use UXF\Hydrator\ParameterGenerator;

final readonly class MoneyParameterGenerator implements ParameterGenerator
{
    public function generate(ParameterDefinition $definition, Options $options): string
    {
        $name = $definition->name;

        $body = '// ' . __CLASS__ . "\n";

        if (!$definition->array) {
            $body .= "if (\$data[\$name] instanceof \\" . Money::class . ") {\n";
            $body .= "    \$_$name = \$data[\$name];\n";
            $body .= "} elseif (isset(\$data[\$name]['amount'], \$data[\$name]['currency'])) {\n";
            $body .= "    \$moneyError = false;\n";
            $body .= "    if (!is_string(\$data[\$name]['amount']) || preg_match('/^(\-)?\d+(\.\d+)?$/', \$data[\$name]['amount']) === 0) {\n";
            $body .= "        \$errors[\$path . \$name . '.amount'][] = \$this->translator->trans('money.invalid_amount_format', new ErrorInfo(\$data[\$name]['amount'], supportedFormat: '-100.00'));\n";
            $body .= "        \$moneyError = true;\n";
            $body .= "    }\n";
            $body .= "    \$currency = is_string(\$data[\$name]['currency']) ? \\" . Currency::class . "::tryFrom(\$data[\$name]['currency']) : null;\n";
            $body .= "    if (\$currency === null) {\n";
            $body .= "        \$errors[\$path . \$name . '.currency'][] = \$this->translator->trans('money.invalid_currency_format', new ErrorInfo(\$data[\$name]['currency'], supportedFormat: 'ISO 4217'));\n";
            $body .= "        \$moneyError = true;\n";
            $body .= "    }\n";
            $body .= "    if (!\$moneyError) {\n";
            $body .= "        \$_$name = \\" . Money::class . "::of(\$data[\$name]['amount'], \$currency);\n";
            $body .= "    }\n";
        } else {
            $body .= "if (is_array(\$data[\$name])) {\n";
            $body .= "    \$_$name = [];\n";
            $body .= "    \$isList = array_is_list(\$data[\$name]);\n";
            $body .= "    foreach (\$data[\$name] as \$key => \$value) {\n";
            $body .= "        \$xPath = \$isList ? \"{\$name}[\$key]\" : \"{\$name}.\$key\";\n";
            $body .= "        if (\$value instanceof \\" . Money::class . ") {\n";
            $body .= "            \$_{$name}[\$key] = \$value;\n";
            $body .= "        } elseif (isset(\$value['amount'], \$value['currency'])) {\n";
            $body .= "            \$moneyError = false;\n";
            $body .= "            if (!is_string(\$value['amount']) || preg_match('/^(\-)?\d+(\.\d+)?$/', \$value['amount']) === 0) {\n";
            $body .= "                \$errors[\$path . \$xPath . '.amount'][] = \$this->translator->trans('money.invalid_amount_format', new ErrorInfo(\$value['amount'], supportedFormat: '-100.00'));\n";
            $body .= "                \$moneyError = true;\n";
            $body .= "            }\n";
            $body .= "            \$currency = is_string(\$value['currency']) ? \\" . Currency::class . "::tryFrom(\$value['currency']) : null;\n";
            $body .= "            if (\$currency === null) {\n";
            $body .= "                \$errors[\$path . \$xPath . '.currency'][] = \$this->translator->trans('money.invalid_currency_format', new ErrorInfo(\$value['currency'], supportedFormat: 'ISO 4217'));\n";
            $body .= "                \$moneyError = true;\n";
            $body .= "            }\n";
            $body .= "            if (!\$moneyError) {\n";
            $body .= "                \$_{$name}[\$key] = \\" . Money::class . "::of(\$value['amount'], \$currency);\n";
            $body .= "            }\n";
            $body .= "        } else {\n";
            $body .= "            \$errors[\$path . \$xPath][] = \$this->translator->trans('core.invalid_value', new ErrorInfo(\$value));\n";
            $body .= "        }\n";
            $body .= "    }\n";
        }

        if ($definition->nullable) {
            $body .= "} elseif (\$data[\$name] === null) {\n";
            $body .= "    \$_$name = null;\n";
        }

        $msg = $definition->array ? 'array_invalid_value' : 'invalid_value';

        $body .= "} else {\n";
        $body .= "    \$errors[\$path . \$name][] = \$this->translator->trans('core.$msg', new ErrorInfo(\$data[\$name]));\n";
        $body .= "}\n";

        return $body;
    }

    public function supports(ParameterDefinition $definition, Options $options): bool
    {
        return !$definition->isUnion() && $definition->getFirstType() === Money::class;
    }

    public static function getDefaultPriority(): int
    {
        return 200;
    }
}

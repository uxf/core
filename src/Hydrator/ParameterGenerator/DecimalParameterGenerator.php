<?php

declare(strict_types=1);

namespace UXF\Core\Hydrator\ParameterGenerator;

use UXF\Core\Type\Decimal;
use UXF\Hydrator\Generator\GeneratorHelper;
use UXF\Hydrator\Inspector\ParameterDefinition;
use UXF\Hydrator\Options;
use UXF\Hydrator\ParameterGenerator;

final readonly class DecimalParameterGenerator implements ParameterGenerator
{
    public function generate(ParameterDefinition $definition, Options $options): string
    {
        return GeneratorHelper::of(
            generatorClass: __CLASS__,
            definition: $definition,
            errorMsgKey: 'decimal.invalid_format',
            errorArgs: "supportedFormat: '-100.00'",
        );
    }

    public function supports(ParameterDefinition $definition, Options $options): bool
    {
        return !$definition->isUnion() && $definition->getFirstType() === Decimal::class;
    }

    public static function getDefaultPriority(): int
    {
        return 200;
    }
}

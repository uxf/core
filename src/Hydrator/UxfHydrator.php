<?php

declare(strict_types=1);

namespace UXF\Core\Hydrator;

use UXF\Hydrator\ObjectHydrator;

final readonly class UxfHydrator implements Hydrator
{
    public function __construct(private ObjectHydrator $hydrator)
    {
    }

    /**
     * {@inheritDoc}
     */
    public function hydrateArray(array $data, string $class): mixed
    {
        return $this->hydrator->hydrateArray($data, $class);
    }

    /**
     * {@inheritDoc}
     */
    public function hydrateArrays(array $data, string $class): array
    {
        return $this->hydrator->hydrateArrays($data, $class);
    }
}

<?php

declare(strict_types=1);

namespace UXF\Core\PHPStanRules;

use PhpParser\Node;
use PhpParser\Node\Expr\New_;
use PHPStan\Analyser\Scope;
use PHPStan\Reflection\ReflectionProvider;
use PHPStan\Rules\Rule;
use ReflectionException;

/**
 * @implements Rule<New_>
 */
final class InternalEnforcementNewRule implements Rule
{
    public function __construct(
        private ReflectionProvider $reflectionProvider,
    ) {
    }

    public function getNodeType(): string
    {
        return New_::class;
    }

    /**
     * @inheritDoc
     */
    public function processNode(Node $node, Scope $scope): array
    {
        if ($node->class instanceof Node\Name) {
            $methodClass = (string)$node->class;
        } else {
            return []; // magic
        }

        if (!class_exists($methodClass)) {
            return [];
        }

        $classReflection = $this->reflectionProvider->getClass($methodClass);

        try {
            $reflectionMethod = $classReflection->getNativeReflection()->getConstructor();
        } catch (ReflectionException) {
            return [];
        }

        // without constructor
        if ($reflectionMethod === null) {
            return [];
        }

        return InternalEnforcementCheck::check($reflectionMethod, $scope);
    }
}

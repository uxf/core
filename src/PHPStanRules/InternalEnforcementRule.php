<?php

declare(strict_types=1);

namespace UXF\Core\PHPStanRules;

use PhpParser\Node;
use PhpParser\Node\Expr\MethodCall;
use PhpParser\Node\Identifier;
use PHPStan\Analyser\Scope;
use PHPStan\Rules\Rule;
use ReflectionException;

/**
 * @implements Rule<MethodCall>
 */
final class InternalEnforcementRule implements Rule
{
    public function getNodeType(): string
    {
        return MethodCall::class;
    }

    /**
     * @inheritDoc
     */
    public function processNode(Node $node, Scope $scope): array
    {
        $type = $scope->getType($node->var);

        if ($type->isObject()->no()) {
            return [];
        }

        $methodClass = $type->getObjectClassNames()[0] ?? '';

        if (!($node->name instanceof Identifier) || !class_exists($methodClass)) {
            return [];
        }

        $methodName = $node->name->name;

        try {
            $ref = $type->getObjectClassReflections()[0];
            $reflectionMethod = $ref->getNativeReflection()->getMethod($methodName);
        } catch (ReflectionException) {
            return [];
        }

        return InternalEnforcementCheck::check($reflectionMethod, $scope);
    }
}

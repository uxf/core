<?php

declare(strict_types=1);

namespace UXF\Core\PHPStanRules;

use LogicException;
use PHPStan\Analyser\Scope;
use PHPStan\Rules\IdentifierRuleError;
use PHPStan\Rules\RuleErrorBuilder;
use ReflectionMethod;
use UXF\Core\Attribute\Internal;

final readonly class InternalEnforcementCheck
{
    /**
     * @return list<IdentifierRuleError>
     */
    public static function check(ReflectionMethod $reflectionMethod, Scope $scope): array
    {
        $methodAttributes = $reflectionMethod->getAttributes(Internal::class);
        if ($methodAttributes === []) {
            return [];
        }

        $callerClassName = $scope->getClassReflection()?->getName() ?? throw new LogicException();
        $attribute = $methodAttributes[0]->newInstance();

        // allow call from tests
        if (str_starts_with($callerClassName, 'App\Tests\\')) {
            return [];
        }

        if ($callerClassName === $attribute->className) {
            return [];
        }

        $methodClass = $reflectionMethod->getDeclaringClass()->getName();
        $methodName = $reflectionMethod->getName();

        $msg = "⚠️ Internal violation: $methodClass::$methodName() is called from forbidden place";
        return [RuleErrorBuilder::message($msg)->identifier('uxf')->build()];
    }
}

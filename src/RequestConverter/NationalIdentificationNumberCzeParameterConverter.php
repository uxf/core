<?php

declare(strict_types=1);

namespace UXF\Core\RequestConverter;

use Exception;
use UXF\Core\Exception\ValidationException;
use UXF\Core\Type\NationalIdentificationNumberCze;

final readonly class NationalIdentificationNumberCzeParameterConverter
{
    public static function convert(mixed $value, string $name): NationalIdentificationNumberCze
    {
        try {
            return NationalIdentificationNumberCze::of($value);
        } catch (Exception $e) {
            throw new ValidationException([[
                'field' => "url:$name",
                'message' => $e->getMessage(),
            ]]);
        }
    }
}

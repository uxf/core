<?php

declare(strict_types=1);

namespace UXF\Core\RequestConverter;

use Exception;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use UXF\Core\Exception\ValidationException;

final readonly class UuidParameterConverter
{
    public static function convert(mixed $value, string $name): UuidInterface
    {
        try {
            return Uuid::fromString($value);
        } catch (Exception $e) {
            throw new ValidationException([[
                'field' => "url:$name",
                'message' => $e->getMessage(),
            ]]);
        }
    }
}

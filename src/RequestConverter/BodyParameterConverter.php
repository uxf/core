<?php

declare(strict_types=1);

namespace UXF\Core\RequestConverter;

use LogicException;
use Nette\Utils\Json;
use Nette\Utils\JsonException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use UXF\Core\Exception\BasicException;
use UXF\Core\Exception\ValidationException;
use UXF\Core\Hydrator\Hydrator;
use UXF\Hydrator\Exception\HydratorCollectionException;
use UXF\Hydrator\Exception\HydratorException;

final readonly class BodyParameterConverter
{
    public function __construct(
        private Hydrator $hydrator,
        private ValidatorInterface $validator,
    ) {
    }

    public function convert(Request $request, string $className, bool $isArray): mixed
    {
        $content = $request->getContent();

        try {
            $data = Json::decode($content, forceArrays: true);
        } catch (JsonException $e) {
            throw new BasicException($e->getMessage());
        }

        if ($className === 'mixed') {
            return $data;
        }

        if (!class_exists($className) && !interface_exists($className)) {
            throw new LogicException('Invalid FromBody type');
        }

        if (!is_array($data)) {
            throw new BasicException('Invalid request body (JSON looks like string - double encode?)');
        }

        try {
            $result = $isArray ? $this->hydrator->hydrateArrays($data, $className) : $this->hydrator->hydrateArray($data, $className);
        } catch (HydratorCollectionException $e) {
            throw ValidationException::fromHydratorCollectionException($e);
        } catch (HydratorException $e) {
            throw ValidationException::fromHydratorErrors($e->errors);
        }

        $e = ValidationException::fromValidator($this->validator, $result);
        if ($e !== null) {
            throw $e;
        }

        return $result;
    }
}

<?php

declare(strict_types=1);

namespace UXF\Core\RequestConverter;

use Symfony\Component\HttpFoundation\Request;
use UXF\Core\Exception\ValidationException;
use UXF\Core\Hydrator\Hydrator;
use UXF\Hydrator\Exception\HydratorException;

final readonly class HeaderParameterConverter
{
    public function __construct(
        private Hydrator $hydrator,
    ) {
    }

    /**
     * @phpstan-param class-string $className
     */
    public function convert(Request $request, string $className): mixed
    {
        $data = [];
        $headers = $request->headers->all();
        foreach ($headers as $name => $values) {
            $data[self::camelize($name)] = $values[0] ?? null;
        }

        try {
            return $this->hydrator->hydrateArray($data, $className);
        } catch (HydratorException $e) {
            throw ValidationException::fromHydratorErrors($e->errors, 'header:');
        }
    }

    private static function camelize(string $input): string
    {
        return lcfirst(str_replace('-', '', ucwords(mb_strtolower($input), '-')));
    }
}

<?php

declare(strict_types=1);

namespace UXF\Core\RequestConverter;

use Doctrine\ORM\EntityManagerInterface;
use ReflectionEnum;
use ReflectionMethod;
use ReflectionNamedType;
use ReflectionUnionType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use UXF\Core\Exception\BasicException;
use UXF\Core\Exception\ValidationException;
use UXF\Core\Hydrator\Hydrator;
use UXF\Core\Utils\SH;
use UXF\Hydrator\Exception\HydratorException;
use UXF\Hydrator\Inspector\ArrayPhpDocParser;

final readonly class QueryParameterConverter
{
    public function __construct(
        private Hydrator $hydrator,
        private ValidatorInterface $validator,
        private ArrayPhpDocParser $arrayPhpDocParser,
        private ?EntityManagerInterface $entityManager = null,
    ) {
    }

    /**
     * @phpstan-param class-string $className
     */
    public function convert(Request $request, string $className): mixed
    {
        $data = $request->query->all();

        // magic conversion string -> right type
        $data = $this->retype($data, $className);

        try {
            $result = $this->hydrator->hydrateArray($data, $className);
        } catch (HydratorException $e) {
            throw ValidationException::fromHydratorErrors($e->errors, 'query:');
        }

        $e = ValidationException::fromValidator($this->validator, $result);
        if ($e !== null) {
            throw $e;
        }

        return $result;
    }

    /**
     * @internal
     */
    public function retype(mixed $data, string $className): mixed
    {
        if (is_string($data) && enum_exists($className) && (string) (new ReflectionEnum($className))->getBackingType() === 'int') {
            return SH::s2i($data);
        }

        if (class_exists($className) && $data === '' && $this->entityManager?->getMetadataFactory()->isTransient($className) === false) {
            return null;
        }

        if (!is_array($data) || !class_exists($className) || enum_exists($className)) {
            return $data;
        }

        // TODO ReflectionException
        $reflectionMethod = new ReflectionMethod($className, '__construct');

        $parameters = [];
        foreach ($reflectionMethod->getParameters() as $parameter) {
            $parameters[$parameter->name] = $parameter;
        }

        $result = [];
        foreach ($data as $key => $value) {
            $parameter = $parameters[$key] ?? null;
            if ($parameter === null) {
                continue;
            }

            $refType = $parameter->getType();
            $paramType = match (true) {
                $refType instanceof ReflectionNamedType => $refType->getName(),
                $refType instanceof ReflectionUnionType => $refType->getTypes()[0]->getName(),
                default => null,
            };

            try {
                $result[$key] = match ($paramType) {
                    'string' => $value,
                    'int' => SH::s2i($value),
                    'float' => SH::s2f($value),
                    'bool' => SH::s2b($value),
                    'array' => self::retypeArray(
                        $value,
                        $this->arrayPhpDocParser->resolveTypes($parameter)?->types[0] ?? 'string', // TODO
                    ),
                    null => null,
                    default => $this->retype($value, $paramType),
                };
            } catch (BasicException) {
                // TODO - is it ok?
                if ($parameter->allowsNull()) {
                    $result[$key] = null;
                } else {
                    unset($result[$key]);
                }
            }
        }

        return $result;
    }

    /**
     * @param string|array<mixed> $value
     */
    private static function retypeArray(string|array $value, string $type): mixed
    {
        if (is_string($value)) {
            return $value;
        }

        $result = [];
        foreach ($value as $key => $v) {
            $result[$key] = match ($type) {
                'int' => SH::s2i($v),
                'float' => SH::s2f($v),
                'bool' => SH::s2b($v),
                default => $v,
            };
        }
        return $result;
    }
}

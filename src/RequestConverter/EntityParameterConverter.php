<?php

declare(strict_types=1);

namespace UXF\Core\RequestConverter;

use Doctrine\ORM\EntityManagerInterface;
use UXF\Core\Exception\BasicException;

final readonly class EntityParameterConverter
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    /**
     * @phpstan-param class-string&literal-string $class
     * @phpstan-param literal-string $property
     */
    public function convert(mixed $value, string $class, ?string $property): ?object
    {
        if (is_object($value) || $value === null) {
            return $value;
        }

        $object = $property === null
            ? $this->entityManager->find($class, $value)
            : $this->entityManager->createQueryBuilder()
            ->select('entity')
            ->from($class, 'entity')
            ->where("entity.$property = :id")
            ->setParameter('id', $value)
            ->getQuery()
            ->getOneOrNullResult();

        return $object ?? throw BasicException::notFound();
    }

    /**
     * @phpstan-param class-string $class
     */
    public function support(string $class): bool
    {
        return !$this->entityManager->getMetadataFactory()->isTransient($class);
    }
}

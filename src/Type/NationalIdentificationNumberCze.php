<?php

declare(strict_types=1);

namespace UXF\Core\Type;

use Exception;
use JsonSerializable;
use LogicException;
use Stringable;
use UXF\Core\SystemProvider\Calendar;

// experimental
final readonly class NationalIdentificationNumberCze implements JsonSerializable, Stringable, Equals
{
    private function __construct(
        private string $value,
    ) {
    }

    public static function of(string $value): self
    {
        // https://phpfashion.com/jak-overit-platne-ic-a-rodne-cislo
        // be liberal in what you receive
        if (preg_match('#^\s*(\d\d)(\d\d)(\d\d)[ /]*(\d\d\d)(\d?)\s*$#', $value, $matches) !== 1) {
            throw new Exception('Neplatné rodné číslo');
        }

        [, $year, $month, $day, $ext, $c] = $matches;
        $originalYear = (int) $year;
        $originalMonth = (int) $month;

        if ($c === '') {
            $year = (int) $year;
            $year += $year < 54 ? 1900 : 1800;
        } else {
            // kontrolní číslice
            $mod = (int) ($year . $month . $day . $ext) % 11;
            if ($mod === 10) {
                $mod = 0;
            }
            if ($mod !== (int) $c) {
                throw new Exception('Neplatné rodné číslo');
            }

            $year = (int) $year;
            $year += $year < 54 ? 2000 : 1900;
        }

        $month = (int) $month;
        $day = (int) $day;

        // k měsíci může být připočteno 20, 50 nebo 70
        if ($month > 70 && $year > 2003) {
            $month -= 70;
        } elseif ($month > 50) {
            $month -= 50;
        } elseif ($month > 20 && $year > 2003) {
            $month -= 20;
        }

        // kontrola data
        if (!checkdate($month, $day, $year)) {
            throw new Exception('Neplatné rodné číslo');
        }

        return new self(sprintf('%02d%02d%02d/%03d%s', $originalYear, $originalMonth, $day, $ext, $c));
    }

    public static function tryParse(string $value): ?self
    {
        try {
            return self::of($value);
        } catch (Exception) {
            return null;
        }
    }

    public static function parseNullable(?string $value): ?self
    {
        if ($value === null) {
            return null;
        }

        return self::of($value);
    }

    public function getBirthDate(): Date
    {
        $parts = $this->getParts();
        if (preg_match('/^(\d\d)(\d\d)(\d\d)$/', $parts[0], $matches) !== 1) {
            throw new LogicException('Neplatné rodné číslo');
        }

        [, $year, $month, $day] = $matches;
        $year = (int) $year;
        $month = (int) $month;

        if (strlen($parts[1]) < 4) {
            $year += $year < 54 ? 1900 : 1800;
        } else {
            $year += $year < 54 ? 2000 : 1900;
        }

        // k měsíci může být připočteno 20, 50 nebo 70
        if ($month > 70 && $year > 2003) {
            $month -= 70;
        } elseif ($month > 50) {
            $month -= 50;
        } elseif ($month > 20 && $year > 2003) {
            $month -= 20;
        }

        return Date::createFromFormat('Y-n-j', "$year-$month-$day");
    }

    public function getAge(): int
    {
        return Calendar::today()->diff($this->getBirthDate())->y;
    }

    public function withoutSlash(): string
    {
        return str_replace('/', '', $this->value);
    }

    public function beforeSlash(): string
    {
        return $this->getParts()[0];
    }

    public function afterSlash(): string
    {
        return $this->getParts()[1];
    }

    public function toString(): string
    {
        return $this->value;
    }

    /**
     * pouziti pro doctrine
     * @internal
     */
    public static function createFromTrustedSource(string $value): self
    {
        return new self($value);
    }

    public function equals(?self $other): bool
    {
        return $this->value === $other?->value;
    }

    public function any(self ...$others): bool
    {
        foreach ($others as $other) {
            if ($other->equals($this)) {
                return true;
            }
        }

        return false;
    }

    public function jsonSerialize(): string
    {
        return $this->value;
    }

    public function __toString(): string
    {
        return $this->value;
    }

    /**
     * @return array{string, string}
     */
    private function getParts(): array
    {
        $parts = explode('/', $this->value);
        return [$parts[0], $parts[1] ?? ''];
    }
}

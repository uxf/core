<?php

declare(strict_types=1);

namespace UXF\Core\Type;

use Exception;
use JsonSerializable;
use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use Stringable;

final class Phone implements JsonSerializable, Stringable, Equals
{
    private static ?PhoneNumberUtil $phoneUtil = null;

    // E.164 format
    private function __construct(
        private readonly string $value,
    ) {
    }

    public static function of(string $value, string $defaultRegion = 'CZ'): self
    {
        self::$phoneUtil ??= PhoneNumberUtil::getInstance();

        try {
            $phoneNumber = self::$phoneUtil->parse($value, $defaultRegion);
        } catch (NumberParseException $e) {
            throw new Exception('Invalid phone number', previous: $e);
        }

        if (!self::$phoneUtil->isValidNumber($phoneNumber)) {
            // second try - SK
            $phoneNumber = self::$phoneUtil->parse($value, 'SK');
            if (!self::$phoneUtil->isValidNumber($phoneNumber)) {
                throw new Exception('Invalid phone number');
            }
        }

        return new self(self::$phoneUtil->format($phoneNumber, PhoneNumberFormat::E164));
    }

    /**
     * @deprecated - use Phone::of()
     */
    public static function parse(string $value): self
    {
        return self::of($value);
    }

    public static function parseNullable(?string $value): ?self
    {
        if ($value === null) {
            return null;
        }
        return self::of($value);
    }

    public static function tryParse(?string $value): ?self
    {
        if ($value === null) {
            return null;
        }

        try {
            self::$phoneUtil ??= PhoneNumberUtil::getInstance();
            $phoneNumber = self::$phoneUtil->parse($value, 'CZ');

            if (!self::$phoneUtil->isValidNumber($phoneNumber)) {
                return null;
            }

            return new self(self::$phoneUtil->format($phoneNumber, PhoneNumberFormat::E164));
        } catch (NumberParseException) {
            return null;
        }
    }

    /**
     * pouziti pro doctrine
     * @internal
     */
    public static function createFromTrustedSource(string $value): self
    {
        return new self($value);
    }

    public function equals(?self $other): bool
    {
        return $this->value === $other?->value;
    }

    public function any(self ...$others): bool
    {
        foreach ($others as $other) {
            if ($other->equals($this)) {
                return true;
            }
        }

        return false;
    }

    public function getCountryCode(): int
    {
        return (self::$phoneUtil ??= PhoneNumberUtil::getInstance())->parse($this->value)->getCountryCode() ?? throw new Exception('CountryCode is null');
    }

    public function getNationalNumber(): string
    {
        return (self::$phoneUtil ??= PhoneNumberUtil::getInstance())->parse($this->value)->getNationalNumber() ?? throw new Exception('NationalNumber is null');
    }

    public function toString(): string
    {
        return $this->value;
    }

    public function __toString(): string
    {
        return $this->value;
    }

    /**
     * @return array<mixed>
     */
    public function __serialize(): array
    {
        return [
            'value' => $this->value,
        ];
    }

    /**
     * @param array<mixed> $data
     */
    public function __unserialize(array $data): void
    {
        $this->value = $data['value'];
    }

    public function jsonSerialize(): string
    {
        return $this->value;
    }
}

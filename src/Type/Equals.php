<?php

declare(strict_types=1);

namespace UXF\Core\Type;

/**
 * @method bool equals(?self $other)
 */
interface Equals
{
}

<?php

declare(strict_types=1);

namespace UXF\Core\Type;

final readonly class UrlComponents
{
    /**
     * @param array<mixed> $query
     */
    public function __construct(
        public string $scheme,
        public string $host,
        public ?int $port,
        public ?string $user,
        public ?string $pass,
        public ?string $path,
        public array $query,
        public ?string $fragment,
    ) {
    }
}

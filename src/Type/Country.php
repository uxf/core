<?php

declare(strict_types=1);

namespace UXF\Core\Type;

use Exception;
use UXF\Core\Attribute\Label;
use UXF\Core\Attribute\LabelTrait;

enum Country: string
{
    use LabelTrait;

    #[Label('Česká republika')]
    case Czechia = 'CZ';
    #[Label('Slovensko')]
    case Slovakia = 'SK';
    #[Label('Polsko')]
    case Poland = 'PL';
    #[Label('Německo')]
    case Germany = 'DE';
    #[Label('Rakousko')]
    case Austria = 'AT';
    #[Label('Afghánistán')]
    case Afghanistan = 'AF';
    #[Label('Albánie')]
    case Albania = 'AL';
    #[Label('Alžírsko')]
    case Algeria = 'DZ';
    #[Label('Americká Samoa')]
    case AmericanSamoa = 'AS';
    #[Label('Americké Panenské ostrovy')]
    case USVirginIslands = 'VI';
    #[Label('Andorra')]
    case Andorra = 'AD';
    #[Label('Angola')]
    case Angola = 'AO';
    #[Label('Anguilla')]
    case Anguilla = 'AI';
    #[Label('Antarktida')]
    case Antarctica = 'AQ';
    #[Label('Antigua a Barbuda')]
    case AntiguaAndBarbuda = 'AG';
    #[Label('Argentina')]
    case Argentina = 'AR';
    #[Label('Arménie')]
    case Armenia = 'AM';
    #[Label('Aruba')]
    case Aruba = 'AW';
    #[Label('Austrálie')]
    case Australia = 'AU';
    #[Label('Ázerbajdžán')]
    case Azerbaijan = 'AZ';
    #[Label('Bahamy')]
    case Bahamas = 'BS';
    #[Label('Bahrajn')]
    case Bahrain = 'BH';
    #[Label('Bangladéš')]
    case Bangladesh = 'BD';
    #[Label('Barbados')]
    case Barbados = 'BB';
    #[Label('Belgie')]
    case Belgium = 'BE';
    #[Label('Belize')]
    case Belize = 'BZ';
    #[Label('Bělorusko')]
    case Belarus = 'BY';
    #[Label('Benin')]
    case Benin = 'BJ';
    #[Label('Bermudy')]
    case Bermuda = 'BM';
    #[Label('Bhútán')]
    case Bhutan = 'BT';
    #[Label('Bolívie')]
    case Bolivia = 'BO';
    #[Label('Bosna a Hercegovina')]
    case BosniaAndHerzegovina = 'BA';
    #[Label('Botswana')]
    case Botswana = 'BW';
    #[Label('Brazílie')]
    case Brazil = 'BR';
    #[Label('Britské indickooceánské území')]
    case BritishIndianOceanTerritory = 'IO';
    #[Label('Britské Panenské ostrovy')]
    case BritishVirginIslands = 'VG';
    #[Label('Brunej')]
    case Brunei = 'BN';
    #[Label('Bulharsko')]
    case Bulgaria = 'BG';
    #[Label('Burkina Faso')]
    case BurkinaFaso = 'BF';
    #[Label('Burundi')]
    case Burundi = 'BI';
    #[Label('Chile')]
    case Chile = 'CL';
    #[Label('Chorvatsko')]
    case Croatia = 'HR';
    #[Label('Cookovy ostrovy')]
    case CookIslands = 'CK';
    #[Label('Curacao')]
    case Curacao = 'CW';
    #[Label('Čad')]
    case Chad = 'TD';
    #[Label('Černá Hora')]
    case Montenegro = 'ME';
    #[Label('Čína')]
    case China = 'CN';
    #[Label('Dánsko')]
    case Denmark = 'DK';
    #[Label('Demokratická republika Kongo')]
    case DemocraticRepublicOfTheCongo = 'CD';
    #[Label('Dominika')]
    case Dominica = 'DM';
    #[Label('Dominikánská republika')]
    case DominicanRepublic = 'DO';
    #[Label('Džibuti')]
    case Djibouti = 'DJ';
    #[Label('Egypt')]
    case Egypt = 'EG';
    #[Label('Ekvádor')]
    case Ecuador = 'EC';
    #[Label('El Salvador')]
    case ElSalvador = 'SV';
    #[Label('Eritrea')]
    case Eritrea = 'ER';
    #[Label('Estonsko')]
    case Estonia = 'EE';
    #[Label('Etiopie')]
    case Ethiopia = 'ET';
    #[Label('Faerské ostrovy')]
    case FaroeIslands = 'FO';
    #[Label('Falklandy')]
    case FalklandIslands = 'FK';
    #[Label('Fidži')]
    case Fiji = 'FJ';
    #[Label('Filipíny')]
    case Philippines = 'PH';
    #[Label('Finsko')]
    case Finland = 'FI';
    #[Label('Francie')]
    case France = 'FR';
    #[Label('Francouzská Polynésie')]
    case FrenchPolynesia = 'PF';
    #[Label('Gabon')]
    case Gabon = 'GA';
    #[Label('Gambie')]
    case Gambia = 'GM';
    #[Label('Ghana')]
    case Ghana = 'GH';
    #[Label('Gibraltar')]
    case Gibraltar = 'GI';
    #[Label('Grenada')]
    case Grenada = 'GD';
    #[Label('Grónsko')]
    case Greenland = 'GL';
    #[Label('Gruzie')]
    case Georgia = 'GE';
    #[Label('Guam')]
    case Guam = 'GU';
    #[Label('Guatemala')]
    case Guatemala = 'GT';
    #[Label('Guernsey')]
    case Guernsey = 'GG';
    #[Label('Guinea')]
    case Guinea = 'GN';
    #[Label('Guinea-Bissau')]
    case Bissau = 'GW';
    #[Label('Guyana')]
    case Guyana = 'GY';
    #[Label('Haiti')]
    case Haiti = 'HT';
    #[Label('Honduras')]
    case Honduras = 'HN';
    #[Label('Hongkong')]
    case HongKong = 'HK';
    #[Label('Indie')]
    case India = 'IN';
    #[Label('Indonésie')]
    case Indonesia = 'ID';
    #[Label('Irák')]
    case Iraq = 'IQ';
    #[Label('Írán')]
    case Iran = 'IR';
    #[Label('Irsko')]
    case Ireland = 'IE';
    #[Label('Island')]
    case Iceland = 'IS';
    #[Label('Itálie')]
    case Italy = 'IT';
    #[Label('Izrael')]
    case Israel = 'IL';
    #[Label('Jamajka')]
    case Jamaica = 'JM';
    #[Label('Japonsko')]
    case Japan = 'JP';
    #[Label('Jemen')]
    case Yemen = 'YE';
    #[Label('Jersey')]
    case Jersey = 'JE';
    #[Label('Jižní Afrika')]
    case SouthAfrica = 'ZA';
    #[Label('Jižní Korea')]
    case SouthKorea = 'KR';
    #[Label('jižní Súdán')]
    case SouthSudan = 'SS';
    #[Label('Jordánsko')]
    case Jordan = 'JO';
    #[Label('Kajmanské ostrovy')]
    case CaymanIslands = 'KY';
    #[Label('Kambodža')]
    case Cambodia = 'KH';
    #[Label('Kamerun')]
    case Cameroon = 'CM';
    #[Label('Kanada')]
    case Canada = 'CA';
    #[Label('Kapverdy')]
    case CapeVerde = 'CV';
    #[Label('Katar')]
    case Qatar = 'QA';
    #[Label('Kazachstán')]
    case Kazakhstan = 'KZ';
    #[Label('Keňa')]
    case Kenya = 'KE';
    #[Label('Kiribati')]
    case Kiribati = 'KI';
    #[Label('Kokosové ostrovy')]
    case CocosIslands = 'CC';
    #[Label('Kolumbie')]
    case Colombia = 'CO';
    #[Label('Komory')]
    case Comoros = 'KM';
    #[Label('Konžská demokratická republika Kongo)')]
    case RepublicOfTheCongo = 'CG';
    #[Label('Kostarika')]
    case CostaRica = 'CR';
    #[Label('Kuba')]
    case Cuba = 'CU';
    #[Label('Kuvajt')]
    case Kuwait = 'KW';
    #[Label('Kypr')]
    case Cyprus = 'CY';
    #[Label('Kyrgyzstán')]
    case Kyrgyzstan = 'KG';
    #[Label('Laos')]
    case Laos = 'LA';
    #[Label('Lesotho')]
    case Lesotho = 'LS';
    #[Label('Libanon')]
    case Lebanon = 'LB';
    #[Label('Libérie')]
    case Liberia = 'LR';
    #[Label('Libye')]
    case Libya = 'LY';
    #[Label('Lichtenštejnsko')]
    case Liechtenstein = 'LI';
    #[Label('Litva')]
    case Lithuania = 'LT';
    #[Label('Lotyšsko')]
    case Latvia = 'LV';
    #[Label('Lucembursko')]
    case Luxembourg = 'LU';
    #[Label('Macao')]
    case Macau = 'MO';
    #[Label('Madagaskar')]
    case Madagascar = 'MG';
    #[Label('Maďarsko')]
    case Hungary = 'HU';
    #[Label('Makedonie (Severní Makedonie)')]
    case Macedonia = 'MK';
    #[Label('Malajsie')]
    case Malaysia = 'MY';
    #[Label('Malawi')]
    case Malawi = 'MW';
    #[Label('Maledivy')]
    case Maldives = 'MV';
    #[Label('Mali')]
    case Mali = 'ML';
    #[Label('Malta')]
    case Malta = 'MT';
    #[Label('Man (ostrov)')]
    case IsleOfMan = 'IM';
    #[Label('Maroko')]
    case Morocco = 'MA';
    #[Label('Marshallovy ostrovy')]
    case MarshallIslands = 'MH';
    #[Label('Mauretánie')]
    case Mauritania = 'MR';
    #[Label('Mauricius')]
    case Mauritius = 'MU';
    #[Label('Mayotte')]
    case Mayotte = 'YT';
    #[Label('Mexiko')]
    case Mexico = 'MX';
    #[Label('Mikronésie')]
    case Micronesia = 'FM';
    #[Label('Moldavsko')]
    case Moldova = 'MD';
    #[Label('Monako')]
    case Monaco = 'MC';
    #[Label('Mongolsko')]
    case Mongolia = 'MN';
    #[Label('Montserrat')]
    case Montserrat = 'MS';
    #[Label('Mosambik')]
    case Mozambique = 'MZ';
    #[Label('Myanmar (Barma)')]
    case Myanmar = 'MM';
    #[Label('Namibie')]
    case Namibia = 'NA';
    #[Label('Nauru')]
    case Nauru = 'NR';
    #[Label('Nepál')]
    case Nepal = 'NP';
    #[Label('Niger')]
    case Niger = 'NE';
    #[Label('Nigérie')]
    case Nigeria = 'NG';
    #[Label('Nikaragua')]
    case Nicaragua = 'NI';
    #[Label('Niue')]
    case Niue = 'NU';
    #[Label('Nizozemsko')]
    case Netherlands = 'NL';
    #[Label('Norsko')]
    case Norway = 'NO';
    #[Label('Nová Kaledonie')]
    case NewCaledonia = 'NC';
    #[Label('Nový Zéland')]
    case NewZealand = 'NZ';
    #[Label('Omán')]
    case Oman = 'OM';
    #[Label('Pákistán')]
    case Pakistan = 'PK';
    #[Label('Palau')]
    case Palau = 'PW';
    #[Label('Palestina')]
    case Palestine = 'PS';
    #[Label('Panama')]
    case Panama = 'PA';
    #[Label('Papua-Nová Guinea')]
    case PapuaNewGuinea = 'PG';
    #[Label('Paraguay')]
    case Paraguay = 'PY';
    #[Label('Peru')]
    case Peru = 'PE';
    #[Label('Pitcairnovy ostrovy')]
    case Pitcairn = 'PN';
    #[Label('Pobřeží slonoviny')]
    case IvoryCoast = 'CI';
    #[Label('Portoriko')]
    case PuertoRico = 'PR';
    #[Label('Portugalsko')]
    case Portugal = 'PT';
    #[Label('Réunion')]
    case Reunion = 'RE';
    #[Label('Rovníková Guinea')]
    case EquatorialGuinea = 'GQ';
    #[Label('Rumunsko')]
    case Romania = 'RO';
    #[Label('Rusko')]
    case Russia = 'RU';
    #[Label('Rwanda')]
    case Rwanda = 'RW';
    #[Label('Řecko')]
    case Greece = 'GR';
    #[Label('Saint Pierre a Miquelon')]
    case SaintPierreAndMiquelon = 'PM';
    #[Label('Samoa')]
    case Samoa = 'WS';
    #[Label('San Marino')]
    case SanMarino = 'SM';
    #[Label('Saudská arábie')]
    case SaudiArabia = 'SA';
    #[Label('Senegal')]
    case Senegal = 'SN';
    #[Label('Severní Korea')]
    case NorthKorea = 'KP';
    #[Label('Severní Mariany')]
    case NorthernMarianaIslands = 'MP';
    #[Label('Seychely')]
    case Seychelles = 'SC';
    #[Label('Sierra Leone')]
    case SierraLeone = 'SL';
    #[Label('Singapur')]
    case Singapore = 'SG';
    #[Label('Sint Maarten')]
    case SintMaarten = 'SX';
    #[Label('Slovinsko')]
    case Slovenia = 'SI';
    #[Label('Solomonovy ostrovy')]
    case SolomonIslands = 'SB';
    #[Label('Somálsko')]
    case Somalia = 'SO';
    #[Label('Spojené arabské emiráty')]
    case UnitedArabEmirates = 'AE';
    #[Label('Spojené království')]
    case UnitedKingdom = 'GB';
    #[Label('Spojené státy Americké')]
    case UnitedStates = 'US';
    #[Label('Srbsko')]
    case Serbia = 'RS';
    #[Label('Srí Lanka')]
    case SriLanka = 'LK';
    #[Label('Středoafrická republika')]
    case CentralAfricanRepublic = 'CF';
    #[Label('Súdán')]
    case Sudan = 'SD';
    #[Label('Surinam')]
    case Suriname = 'SR';
    #[Label('Svatá Helena')]
    case SaintHelena = 'SH';
    #[Label('Svatá Lucie')]
    case SaintLucia = 'LC';
    #[Label('Svatý Bartoloměj')]
    case SaintBarthelemy = 'BL';
    #[Label('Svatý Kryštof a Nevis')]
    case SaintKittsAndNevis = 'KN';
    #[Label('Svatý Martin')]
    case SaintMartin = 'MF';
    #[Label('Svatý Tomáš a Princův ostro')]
    case SaoTomeAndPrincipe = 'ST';
    #[Label('Svatý Vincenc a Grenadiny')]
    case SaintVincentAndtheGrenadines = 'VC';
    #[Label('Svazijsko')]
    case Swaziland = 'SZ';
    #[Label('Sýrie')]
    case Syria = 'SY';
    #[Label('Španělsko')]
    case Spain = 'ES';
    #[Label('Špicberky a Jan Mayen')]
    case SvalbardAndJanMayen = 'SJ';
    #[Label('Švédsko')]
    case Sweden = 'SE';
    #[Label('Švýcarsko')]
    case Switzerland = 'CH';
    #[Label('Tádžikistán')]
    case Tajikistan = 'TJ';
    #[Label('Tanzanie')]
    case Tanzania = 'TZ';
    #[Label('Tchaj-wan')]
    case Taiwan = 'TW';
    #[Label('Thajsko')]
    case Thailand = 'TH';
    #[Label('Togo')]
    case Togo = 'TG';
    #[Label('Tokelau')]
    case Tokelau = 'TK';
    #[Label('Tonga')]
    case Tonga = 'TO';
    #[Label('Trinidad a Tobago')]
    case TrinidadAndTobago = 'TT';
    #[Label('Tunisko')]
    case Tunisia = 'TN';
    #[Label('Turecko')]
    case Turkey = 'TR';
    #[Label('Turkmenistán')]
    case Turkmenistan = 'TM';
    #[Label('Turks a Caicos')]
    case TurksAndCaicosIslands = 'TC';
    #[Label('Tuvalu')]
    case Tuvalu = 'TV';
    #[Label('Uganda')]
    case Uganda = 'UG';
    #[Label('Ukrajina')]
    case Ukraine = 'UA';
    #[Label('Uruguay')]
    case Uruguay = 'UY';
    #[Label('Uzbekistán')]
    case Uzbekistan = 'UZ';
    #[Label('Vánoční ostrov')]
    case ChristmasIsland = 'CX';
    #[Label('Vanuatu')]
    case Vanuatu = 'VU';
    #[Label('Vatikán')]
    case Vatican = 'VA';
    #[Label('Venezuela')]
    case Venezuela = 'VE';
    #[Label('Vietnam')]
    case Vietnam = 'VN';
    #[Label('Východní Timor')]
    case EastTimor = 'TL';
    #[Label('Wallis a Futuna')]
    case WallisAndFutuna = 'WF';
    #[Label('Zambie')]
    case Zambia = 'ZM';
    #[Label('Západní Sahara')]
    case WesternSahara = 'EH';
    #[Label('Zimbabwe')]
    case Zimbabwe = 'ZW';

    public function getIsoCode(): string
    {
        return match ($this) {
            self::Czechia => 'CZE',
            self::Slovakia => 'SVK',
            self::Poland => 'POL',
            self::Germany => 'DEU',
            self::Austria => 'AUT',
            self::Afghanistan => 'AFG',
            self::Albania => 'ALB',
            self::Algeria => 'DZA',
            self::AmericanSamoa => 'ASM',
            self::USVirginIslands => 'VIR',
            self::Andorra => 'AND',
            self::Angola => 'AGO',
            self::Anguilla => 'AIA',
            self::Antarctica => 'ATA',
            self::AntiguaAndBarbuda => 'ATG',
            self::Argentina => 'ARG',
            self::Armenia => 'ARM',
            self::Aruba => 'ABW',
            self::Australia => 'AUS',
            self::Azerbaijan => 'AZE',
            self::Bahamas => 'BHS',
            self::Bahrain => 'BHR',
            self::Bangladesh => 'BGD',
            self::Barbados => 'BRB',
            self::Belgium => 'BEL',
            self::Belize => 'BLZ',
            self::Belarus => 'BLR',
            self::Benin => 'BEN',
            self::Bermuda => 'BMU',
            self::Bhutan => 'BTN',
            self::Bolivia => 'BOL',
            self::BosniaAndHerzegovina => 'BIH',
            self::Botswana => 'BWA',
            self::Brazil => 'BRA',
            self::BritishIndianOceanTerritory => 'IOT',
            self::BritishVirginIslands => 'VGB',
            self::Brunei => 'BRN',
            self::Bulgaria => 'BGR',
            self::BurkinaFaso => 'BFA',
            self::Burundi => 'BDI',
            self::Chile => 'CHL',
            self::Croatia => 'HRV',
            self::CookIslands => 'COK',
            self::Curacao => 'CUW',
            self::Chad => 'TCD',
            self::Montenegro => 'MNE',
            self::China => 'CHN',
            self::Denmark => 'DNK',
            self::DemocraticRepublicOfTheCongo => 'COD',
            self::Dominica => 'DMA',
            self::DominicanRepublic => 'DOM',
            self::Djibouti => 'DJI',
            self::Egypt => 'EGY',
            self::Ecuador => 'ECU',
            self::ElSalvador => 'SLV',
            self::Eritrea => 'ERI',
            self::Estonia => 'EST',
            self::Ethiopia => 'ETH',
            self::FaroeIslands => 'FRO',
            self::FalklandIslands => 'FLK',
            self::Fiji => 'FJI',
            self::Philippines => 'PHL',
            self::Finland => 'FIN',
            self::France => 'FRA',
            self::FrenchPolynesia => 'PYF',
            self::Gabon => 'GAB',
            self::Gambia => 'GMB',
            self::Ghana => 'GHA',
            self::Gibraltar => 'GIB',
            self::Grenada => 'GRD',
            self::Greenland => 'GRL',
            self::Georgia => 'GEO',
            self::Guam => 'GUM',
            self::Guatemala => 'GTM',
            self::Guernsey => 'GGY',
            self::Guinea => 'GIN',
            self::Bissau => 'GNB',
            self::Guyana => 'GUY',
            self::Haiti => 'HTI',
            self::Honduras => 'HND',
            self::HongKong => 'HKG',
            self::India => 'IND',
            self::Indonesia => 'IDN',
            self::Iraq => 'IRQ',
            self::Iran => 'IRN',
            self::Ireland => 'IRL',
            self::Iceland => 'ISL',
            self::Italy => 'ITA',
            self::Israel => 'ISR',
            self::Jamaica => 'JAM',
            self::Japan => 'JPN',
            self::Yemen => 'YEM',
            self::Jersey => 'JEY',
            self::SouthAfrica => 'ZAF',
            self::SouthKorea => 'KOR',
            self::SouthSudan => 'SSD',
            self::Jordan => 'JOR',
            self::CaymanIslands => 'CYM',
            self::Cambodia => 'KHM',
            self::Cameroon => 'CMR',
            self::Canada => 'CAN',
            self::CapeVerde => 'CPV',
            self::Qatar => 'QAT',
            self::Kazakhstan => 'KAZ',
            self::Kenya => 'KEN',
            self::Kiribati => 'KIR',
            self::CocosIslands => 'CCK',
            self::Colombia => 'COL',
            self::Comoros => 'COM',
            self::RepublicOfTheCongo => 'COG',
            self::CostaRica => 'CRI',
            self::Cuba => 'CUB',
            self::Kuwait => 'KWT',
            self::Cyprus => 'CYP',
            self::Kyrgyzstan => 'KGZ',
            self::Laos => 'LAO',
            self::Lesotho => 'LSO',
            self::Lebanon => 'LBN',
            self::Liberia => 'LBR',
            self::Libya => 'LBY',
            self::Liechtenstein => 'LIE',
            self::Lithuania => 'LTU',
            self::Latvia => 'LVA',
            self::Luxembourg => 'LUX',
            self::Macau => 'MAC',
            self::Madagascar => 'MDG',
            self::Hungary => 'HUN',
            self::Macedonia => 'MKD',
            self::Malaysia => 'MYS',
            self::Malawi => 'MWI',
            self::Maldives => 'MDV',
            self::Mali => 'MLI',
            self::Malta => 'MLT',
            self::IsleOfMan => 'IMN',
            self::Morocco => 'MAR',
            self::MarshallIslands => 'MHL',
            self::Mauritania => 'MRT',
            self::Mauritius => 'MUS',
            self::Mayotte => 'MYT',
            self::Mexico => 'MEX',
            self::Micronesia => 'FSM',
            self::Moldova => 'MDA',
            self::Monaco => 'MCO',
            self::Mongolia => 'MNG',
            self::Montserrat => 'MSR',
            self::Mozambique => 'MOZ',
            self::Myanmar => 'MMR',
            self::Namibia => 'NAM',
            self::Nauru => 'NRU',
            self::Nepal => 'NPL',
            self::Niger => 'NER',
            self::Nigeria => 'NGA',
            self::Nicaragua => 'NIC',
            self::Niue => 'NIU',
            self::Netherlands => 'NLD',
            self::Norway => 'NOR',
            self::NewCaledonia => 'NCL',
            self::NewZealand => 'NZL',
            self::Oman => 'OMN',
            self::Pakistan => 'PAK',
            self::Palau => 'PLW',
            self::Palestine => 'PSE',
            self::Panama => 'PAN',
            self::PapuaNewGuinea => 'PNG',
            self::Paraguay => 'PRY',
            self::Peru => 'PER',
            self::Pitcairn => 'PCN',
            self::IvoryCoast => 'CIV',
            self::PuertoRico => 'PRI',
            self::Portugal => 'PRT',
            self::Reunion => 'REU',
            self::EquatorialGuinea => 'GNQ',
            self::Romania => 'ROU',
            self::Russia => 'RUS',
            self::Rwanda => 'RWA',
            self::Greece => 'GRC',
            self::SaintPierreAndMiquelon => 'SPM',
            self::Samoa => 'WSM',
            self::SanMarino => 'SMR',
            self::SaudiArabia => 'SAU',
            self::Senegal => 'SEN',
            self::NorthKorea => 'PRK',
            self::NorthernMarianaIslands => 'MNP',
            self::Seychelles => 'SYC',
            self::SierraLeone => 'SLE',
            self::Singapore => 'SGP',
            self::SintMaarten => 'SXM',
            self::Slovenia => 'SVN',
            self::SolomonIslands => 'SLB',
            self::Somalia => 'SOM',
            self::UnitedArabEmirates => 'ARE',
            self::UnitedKingdom => 'GBR',
            self::UnitedStates => 'USA',
            self::Serbia => 'SRB',
            self::SriLanka => 'LKA',
            self::CentralAfricanRepublic => 'CAF',
            self::Sudan => 'SDN',
            self::Suriname => 'SUR',
            self::SaintHelena => 'SHN',
            self::SaintLucia => 'LCA',
            self::SaintBarthelemy => 'BLM',
            self::SaintKittsAndNevis => 'KNA',
            self::SaintMartin => 'MAF',
            self::SaoTomeAndPrincipe => 'STP',
            self::SaintVincentAndtheGrenadines => 'VCT',
            self::Swaziland => 'SWZ',
            self::Syria => 'SYR',
            self::Spain => 'ESP',
            self::SvalbardAndJanMayen => 'SJM',
            self::Sweden => 'SWE',
            self::Switzerland => 'CHE',
            self::Tajikistan => 'TJK',
            self::Tanzania => 'TZA',
            self::Taiwan => 'TWN',
            self::Thailand => 'THA',
            self::Togo => 'TGO',
            self::Tokelau => 'TKL',
            self::Tonga => 'TON',
            self::TrinidadAndTobago => 'TTO',
            self::Tunisia => 'TUN',
            self::Turkey => 'TUR',
            self::Turkmenistan => 'TKM',
            self::TurksAndCaicosIslands => 'TCA',
            self::Tuvalu => 'TUV',
            self::Uganda => 'UGA',
            self::Ukraine => 'UKR',
            self::Uruguay => 'URY',
            self::Uzbekistan => 'UZB',
            self::ChristmasIsland => 'CXR',
            self::Vanuatu => 'VUT',
            self::Vatican => 'VAT',
            self::Venezuela => 'VEN',
            self::Vietnam => 'VNM',
            self::EastTimor => 'TLS',
            self::WallisAndFutuna => 'WLF',
            self::Zambia => 'ZMB',
            self::WesternSahara => 'ESH',
            self::Zimbabwe => 'ZWE',
        };
    }

    public static function fromIsoCode(string $code): self
    {
        return match ($code) {
            'ABW' => self::Aruba,
            'AFG' => self::Afghanistan,
            'AGO' => self::Angola,
            'AIA' => self::Anguilla,
            'ALB' => self::Albania,
            'AND' => self::Andorra,
            'ARE' => self::UnitedArabEmirates,
            'ARG' => self::Argentina,
            'ARM' => self::Armenia,
            'ASM' => self::AmericanSamoa,
            'ATA' => self::Antarctica,
            'ATG' => self::AntiguaAndBarbuda,
            'AUS' => self::Australia,
            'AUT' => self::Austria,
            'AZE' => self::Azerbaijan,
            'BDI' => self::Burundi,
            'BEL' => self::Belgium,
            'BEN' => self::Benin,
            'BFA' => self::BurkinaFaso,
            'BGD' => self::Bangladesh,
            'BGR' => self::Bulgaria,
            'BHR' => self::Bahrain,
            'BHS' => self::Bahamas,
            'BIH' => self::BosniaAndHerzegovina,
            'BLM' => self::SaintBarthelemy,
            'BLR' => self::Belarus,
            'BLZ' => self::Belize,
            'BMU' => self::Bermuda,
            'BOL' => self::Bolivia,
            'BRA' => self::Brazil,
            'BRB' => self::Barbados,
            'BRN' => self::Brunei,
            'BTN' => self::Bhutan,
            'BWA' => self::Botswana,
            'CAF' => self::CentralAfricanRepublic,
            'CAN' => self::Canada,
            'CCK' => self::CocosIslands,
            'CHE' => self::Switzerland,
            'CHL' => self::Chile,
            'CHN' => self::China,
            'CIV' => self::IvoryCoast,
            'CMR' => self::Cameroon,
            'COD' => self::DemocraticRepublicOfTheCongo,
            'COG' => self::RepublicOfTheCongo,
            'COK' => self::CookIslands,
            'COL' => self::Colombia,
            'COM' => self::Comoros,
            'CPV' => self::CapeVerde,
            'CRI' => self::CostaRica,
            'CUB' => self::Cuba,
            'CUW' => self::Curacao,
            'CXR' => self::ChristmasIsland,
            'CYM' => self::CaymanIslands,
            'CYP' => self::Cyprus,
            'CZE' => self::Czechia,
            'DEU' => self::Germany,
            'DJI' => self::Djibouti,
            'DMA' => self::Dominica,
            'DNK' => self::Denmark,
            'DOM' => self::DominicanRepublic,
            'DZA' => self::Algeria,
            'ECU' => self::Ecuador,
            'EGY' => self::Egypt,
            'ERI' => self::Eritrea,
            'ESH' => self::WesternSahara,
            'ESP' => self::Spain,
            'EST' => self::Estonia,
            'ETH' => self::Ethiopia,
            'FIN' => self::Finland,
            'FJI' => self::Fiji,
            'FLK' => self::FalklandIslands,
            'FRA' => self::France,
            'FRO' => self::FaroeIslands,
            'FSM' => self::Micronesia,
            'GAB' => self::Gabon,
            'GBR' => self::UnitedKingdom,
            'GEO' => self::Georgia,
            'GGY' => self::Guernsey,
            'GHA' => self::Ghana,
            'GIB' => self::Gibraltar,
            'GIN' => self::Guinea,
            'GMB' => self::Gambia,
            'GNB' => self::Bissau,
            'GNQ' => self::EquatorialGuinea,
            'GRC' => self::Greece,
            'GRD' => self::Grenada,
            'GRL' => self::Greenland,
            'GTM' => self::Guatemala,
            'GUM' => self::Guam,
            'GUY' => self::Guyana,
            'HKG' => self::HongKong,
            'HND' => self::Honduras,
            'HRV' => self::Croatia,
            'HTI' => self::Haiti,
            'HUN' => self::Hungary,
            'IDN' => self::Indonesia,
            'IMN' => self::IsleOfMan,
            'IND' => self::India,
            'IOT' => self::BritishIndianOceanTerritory,
            'IRL' => self::Ireland,
            'IRN' => self::Iran,
            'IRQ' => self::Iraq,
            'ISL' => self::Iceland,
            'ISR' => self::Israel,
            'ITA' => self::Italy,
            'JAM' => self::Jamaica,
            'JEY' => self::Jersey,
            'JOR' => self::Jordan,
            'JPN' => self::Japan,
            'KAZ' => self::Kazakhstan,
            'KEN' => self::Kenya,
            'KGZ' => self::Kyrgyzstan,
            'KHM' => self::Cambodia,
            'KIR' => self::Kiribati,
            'KNA' => self::SaintKittsAndNevis,
            'KOR' => self::SouthKorea,
            'KWT' => self::Kuwait,
            'LAO' => self::Laos,
            'LBN' => self::Lebanon,
            'LBR' => self::Liberia,
            'LBY' => self::Libya,
            'LCA' => self::SaintLucia,
            'LIE' => self::Liechtenstein,
            'LKA' => self::SriLanka,
            'LSO' => self::Lesotho,
            'LTU' => self::Lithuania,
            'LUX' => self::Luxembourg,
            'LVA' => self::Latvia,
            'MAC' => self::Macau,
            'MAF' => self::SaintMartin,
            'MAR' => self::Morocco,
            'MCO' => self::Monaco,
            'MDA' => self::Moldova,
            'MDG' => self::Madagascar,
            'MDV' => self::Maldives,
            'MEX' => self::Mexico,
            'MHL' => self::MarshallIslands,
            'MKD' => self::Macedonia,
            'MLI' => self::Mali,
            'MLT' => self::Malta,
            'MMR' => self::Myanmar,
            'MNE' => self::Montenegro,
            'MNG' => self::Mongolia,
            'MNP' => self::NorthernMarianaIslands,
            'MOZ' => self::Mozambique,
            'MRT' => self::Mauritania,
            'MSR' => self::Montserrat,
            'MUS' => self::Mauritius,
            'MWI' => self::Malawi,
            'MYS' => self::Malaysia,
            'MYT' => self::Mayotte,
            'NAM' => self::Namibia,
            'NCL' => self::NewCaledonia,
            'NER' => self::Niger,
            'NGA' => self::Nigeria,
            'NIC' => self::Nicaragua,
            'NIU' => self::Niue,
            'NLD' => self::Netherlands,
            'NOR' => self::Norway,
            'NPL' => self::Nepal,
            'NRU' => self::Nauru,
            'NZL' => self::NewZealand,
            'OMN' => self::Oman,
            'PAK' => self::Pakistan,
            'PAN' => self::Panama,
            'PCN' => self::Pitcairn,
            'PER' => self::Peru,
            'PHL' => self::Philippines,
            'PLW' => self::Palau,
            'PNG' => self::PapuaNewGuinea,
            'POL' => self::Poland,
            'PRI' => self::PuertoRico,
            'PRK' => self::NorthKorea,
            'PRT' => self::Portugal,
            'PRY' => self::Paraguay,
            'PSE' => self::Palestine,
            'PYF' => self::FrenchPolynesia,
            'QAT' => self::Qatar,
            'REU' => self::Reunion,
            'ROU' => self::Romania,
            'RUS' => self::Russia,
            'RWA' => self::Rwanda,
            'SAU' => self::SaudiArabia,
            'SDN' => self::Sudan,
            'SEN' => self::Senegal,
            'SGP' => self::Singapore,
            'SHN' => self::SaintHelena,
            'SJM' => self::SvalbardAndJanMayen,
            'SLB' => self::SolomonIslands,
            'SLE' => self::SierraLeone,
            'SLV' => self::ElSalvador,
            'SMR' => self::SanMarino,
            'SOM' => self::Somalia,
            'SPM' => self::SaintPierreAndMiquelon,
            'SRB' => self::Serbia,
            'SSD' => self::SouthSudan,
            'STP' => self::SaoTomeAndPrincipe,
            'SUR' => self::Suriname,
            'SVK' => self::Slovakia,
            'SVN' => self::Slovenia,
            'SWE' => self::Sweden,
            'SWZ' => self::Swaziland,
            'SXM' => self::SintMaarten,
            'SYC' => self::Seychelles,
            'SYR' => self::Syria,
            'TCA' => self::TurksAndCaicosIslands,
            'TCD' => self::Chad,
            'TGO' => self::Togo,
            'THA' => self::Thailand,
            'TJK' => self::Tajikistan,
            'TKL' => self::Tokelau,
            'TKM' => self::Turkmenistan,
            'TLS' => self::EastTimor,
            'TON' => self::Tonga,
            'TTO' => self::TrinidadAndTobago,
            'TUN' => self::Tunisia,
            'TUR' => self::Turkey,
            'TUV' => self::Tuvalu,
            'TWN' => self::Taiwan,
            'TZA' => self::Tanzania,
            'UGA' => self::Uganda,
            'UKR' => self::Ukraine,
            'URY' => self::Uruguay,
            'USA' => self::UnitedStates,
            'UZB' => self::Uzbekistan,
            'VAT' => self::Vatican,
            'VCT' => self::SaintVincentAndtheGrenadines,
            'VEN' => self::Venezuela,
            'VGB' => self::BritishVirginIslands,
            'VIR' => self::USVirginIslands,
            'VNM' => self::Vietnam,
            'VUT' => self::Vanuatu,
            'WLF' => self::WallisAndFutuna,
            'WSM' => self::Samoa,
            'YEM' => self::Yemen,
            'ZAF' => self::SouthAfrica,
            'ZMB' => self::Zambia,
            'ZWE' => self::Zimbabwe,
            default => throw new Exception("Invalid ISO code {$code}"),
        };
    }
}
